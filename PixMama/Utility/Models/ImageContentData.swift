//
//  ImageContentData.swift
//  Uthabo
//
//  Created by UTHABO on 30/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import Foundation
import SwiftyJSON
import Kingfisher

class ImageContentData {
    
    var id : String?
    var title : String?
    var desc: String?
    var tags : [String]?
    var imageURL : String?
    var contributor : ImageContributorData?
    var isFavourite : Bool = false
    var totalLike : Int?
    var imagePricesBySize = [ImageSizePriceData]()
    
    init(jsonData: JSON) {
        
        id = jsonData["id"].string
        title = jsonData["title"].string
        desc = jsonData["description"].string
        tags = jsonData["tags"].arrayObject as? [String]
        imageURL = jsonData["source_link"].string
        contributor = ImageContributorData(jsonData: jsonData["contributor"])
        isFavourite = jsonData["is_favourite"].boolValue
        totalLike = jsonData["total_like"].int
        
        if jsonData["sizes"].dictionaryObject != nil {
            if let sizes = jsonData["sizes"].dictionary {
                for key:String in [String](sizes.keys) {
                    imagePricesBySize.append(ImageSizePriceData(jsonData: sizes[key]!))
                }
            }
        }
    }
}

enum ImageSize: Int {
    
    case Small
    case Medium
    case Large
}

class ImageSizePriceData {
    
    var id: Int?
    var width: CGFloat?
    var height: CGFloat?
    var inchWidth: CGFloat?
    var inchHeight: CGFloat?
    var price: CGFloat?
    var priceUnit: String?
    var dpi: Int?
    var length: CGFloat?
    var group: Group?
    
    init(jsonData: JSON) {
        
        id = jsonData["id"].intValue
        width = CGFloat(jsonData["width"].floatValue)
        height = CGFloat(jsonData["height"].floatValue)
        inchWidth = CGFloat(jsonData["width_in_inch"].floatValue)
        inchHeight = CGFloat(jsonData["height_in_inch"].floatValue)
        price = CGFloat(jsonData["price"].floatValue)
        priceUnit = jsonData["price_unit"].stringValue
        dpi = jsonData["dpi"].intValue
        length = CGFloat(jsonData["length"].floatValue)
        group = Group(jsonData: jsonData["group"])
    }
}

class Group {
    
    var id: Int?
    var name: String?
    var slug: String?
    var imageURL: String?
    
    init(jsonData: JSON) {
        id = jsonData["id"].int ?? Int.min
        name = jsonData["name"].string ?? "No Group Name"
        slug = jsonData["slug"].string ?? "No Group Slug"
        imageURL = jsonData["image_url"].string ?? ""
    }
}
