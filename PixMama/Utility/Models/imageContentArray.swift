//
//  imageContentArray.swift
//  Uthabo
//
//  Created by UTHABO on 30/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import SwiftyJSON

class ImageContentArray {
    
    var resultArray = [ImageContentData]()
    
    init(dataArray: [[String : Any]]?){
        if let data = dataArray {
            for item in data {
                let json = JSON(item)
                resultArray.append(ImageContentData(jsonData: json))
            }
        }
    }
    
}
