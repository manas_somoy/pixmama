//
//  WebViewController.swift
//  PixMama
//
//  Created by UTHABO on 3/12/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import WebKit
import Lottie

class WebViewController: UIViewController, WKNavigationDelegate, UIWebViewDelegate {
    
    var url: String?
    
    var htmlString: String?
    
    var webView: WKWebView!
    
    let loadingView = LOTAnimationView(name: "mamaLoader")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingView.loopAnimation = true
        loadingView.animationSpeed = 2
        
        if let urlString = url {
            if let urlObject = URL(string: urlString) {
                webView.load(URLRequest(url: urlObject))
            }
        }
        
        if let html = htmlString {
            webView.loadHTMLString(html, baseURL: nil)
        }
        
//        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
//        navigationItem.rightBarButtonItems = [refreshButton]
    }
    
    override func loadView() {
        
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        if !loadingView.isDescendant(of: webView) {
            webView.addSubview(loadingView)
            webView.layout(loadingView).center()
            loadingView.play()
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if loadingView.isDescendant(of: view) {
            loadingView.removeFromSuperview()
            loadingView.stop()
        }
    }
}
