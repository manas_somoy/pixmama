//
//  ImageDetailViewController.swift
//  Uthabo
//
//  Created by UTHABO on 30/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import Lightbox
import Lottie

class ImageDetailViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    var aspectRatio: CGFloat?
    var imageHeight: CGFloat?
    var imageWidth = UIScreen.main.bounds.width
    
    var imageData: ImageContentData? {
        didSet {
            let height = CGFloat((imageData?.imagePricesBySize[0].height)!)
            let width = CGFloat((imageData?.imagePricesBySize[0].width)!)
            
            aspectRatio = width / height
            imageHeight = ceil(UIScreen.main.bounds.width / (aspectRatio ?? 0.5))
            photoImageView.frame = CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight ?? 300)
        }
    }
    @IBOutlet weak var imageTableView: UITableView!
    var photoImageView = UIImageView()
    var shadowView: UIView?
    var tapView: UIView?
    
    fileprivate var radioGroup: RadioButtonGroup?
    fileprivate var radioArray = [RadioButton]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationItem()
        prepareTableView()
        prepareImageView()
        
        request?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let imgHeight = imageHeight ?? 300
        let y = imgHeight - (scrollView.contentOffset.y + imgHeight)
        let height = min(max(y, 150), imgHeight*2)
        shadowView?.frame = CGRect(x: 0, y: height, width: imageWidth, height: 0.5)
        photoImageView.frame = CGRect(x: 0, y: 0, width: imageWidth, height: height)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ImageDetailViewController {
    
    fileprivate func prepareTableView() {
        imageTableView.dataSource = self
        imageTableView.delegate = self
        imageTableView.allowsSelection = false
        imageTableView.backgroundColor = UIColor.clear
        imageTableView.contentInset = EdgeInsets(top: imageHeight ?? 300, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate func prepareImageView() {
        photoImageView.contentMode = .scaleAspectFill
        photoImageView.clipsToBounds = true
        if imageHeight! >= imageTableView.frame.size.height - 200 {
            imageHeight = imageTableView.frame.height - 200
        }
        photoImageView.frame = CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight ?? 300)
        
        photoImageView.kf.indicatorType = .custom(indicator: ImageIndicator())
        photoImageView.kf.setImage(with: URL(string: imageData?.imageURL ?? ""))
        view.addSubview(photoImageView)
        
        shadowView = UIView(frame: CGRect(x: 0, y: imageHeight ?? 0, width: imageWidth, height: 0.5))
        shadowView?.backgroundColor = UIColor.black
        shadowView?.depthPreset = .depth3
        view.addSubview(shadowView!)
        
        let imageTap = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
        imageTap.numberOfTapsRequired = 1
        photoImageView.isUserInteractionEnabled = true
        photoImageView.addGestureRecognizer(imageTap)
    }
    
    @objc
    fileprivate func imageTapped(_ sender: UITapGestureRecognizer) {
        if let image = photoImageView.image {
            let image = LightboxImage(image: image, text: imageData?.desc ?? "", videoURL: nil)
            let lightBoxController = LightboxController(images: [image], startIndex: 0)
            lightBoxController.dynamicBackground = true
            present(lightBoxController, animated: true, completion: nil)
        }
    }
    
    fileprivate func prepareNavigationItem() {
        
        navigationItem.titleLabel.text = imageData?.title ?? "Image"
        navigationItem.detailLabel.text = ""
    }
}

extension ImageDetailViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageDetailCell") as? ImageDetailCell
        
        cell?.nameLabel.text = imageData?.title ?? "Image"
        cell?.descLabel.text = imageData?.desc ?? "No description."
        cell?.contributorImageView.kf.setImage(with: URL(string: imageData?.contributor?.imageUrl ?? ""), placeholder: UIImage(named: "iconHomeContributor"), options: [], progressBlock: nil, completionHandler: nil)
        cell?.contributorNameLabel.text = imageData?.contributor?.user_name ?? "Mama User"
        
        
        
        if radioArray.count == 0 {
            
            for prices in (imageData?.imagePricesBySize)! {
                
                let radio = RadioButton()
                radio.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 14)
                radio.titleColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.MotoTextColor)
                radio.titleLabel?.numberOfLines = 0
                radio.titleLabel?.lineBreakMode = .byWordWrapping
                radio.title = String(format: "Pixel Dimension:   %.1f x %.1f\nInch Dimensio:  %.1f x %.1f\nPrice:  %d \(prices.priceUnit ?? "tk")", prices.width ?? 0, prices.height ?? 0, prices.inchWidth ?? 0, prices.inchHeight ?? 0, Int(prices.price ?? 0))
                radio.setIconColor(UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.PinkColor), for: .selected)
                radio.tag = prices.id ?? 0
                radioArray.append(radio)
            }
            
            radioGroup = RadioButtonGroup(buttons: radioArray)
            
            cell?.pricesView.addSubview(radioGroup!)
            radioGroup!.backgroundColor = UIColor.clear
            cell?.pricesView.layout(radioGroup!).topBottom().leadingTrailing().height(CGFloat(90*radioArray.count))
            
        }
        
        cell?.buyButton.addTarget(self, action: #selector(purchasePressed(_:)), for: .touchUpInside)
        
        return cell!
    }
    
    @objc
    fileprivate func purchasePressed(_ sender: UIButton) {
        
        if let sizeId = radioGroup?.selectedButton?.tag, let imageId = imageData?.id {
            
            let url = APIUrls.CREATE_ORDER_URL + "?content_id=\(imageId)" + "&payment_type=OnDemand" + "&desc=Desc" + "&content_feature_id=\(sizeId)" + "&order_type=content" + "&platform=android"
            
            let webVC = WebViewController()
            webVC.url = url
            navigationController?.pushViewController(webVC, animated: true)
        }
    }
}


class ImageDetailCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var contributorImageView: UIImageView!
    @IBOutlet weak var contributorNameLabel: UILabel!
    @IBOutlet weak var pricesView: UIView!
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var likeButton: LoveButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contributorImageView.clipsToBounds = true
        contributorImageView.layer.cornerRadius = contributorImageView.bounds.height/2
        contributorImageView.layer.borderColor = UIColor.hexStringToUIColor(hexString: "dfdfdf").cgColor
        contributorImageView.borderWidthPreset = .border2
        
        buyButton.cornerRadiusPreset = .cornerRadius3
        buyButton.depthPreset = .depth3
        buyButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 18)
        buyButton.setTitle("PURCHASE", for: .normal)
        buyButton.setTitleColor(UIColor.white, for: .normal)
        buyButton.backgroundColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.Green)
    }
    
}

class LoveButton: FlatButton {
    
    var lottieView = LOTAnimationView(name: "love")
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                lottieView.play()
            } else {
                lottieView.play(fromProgress: lottieView.animationProgress, toProgress: 0, withCompletion: nil)
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        
        lottieView.contentMode = .scaleAspectFit
        layout(lottieView).topBottom().leftRight()
        
        self.addTarget(self, action: #selector(touchUpInside(_:)), for: .touchUpInside)
    }
    
    @objc
    func touchUpInside(_ sender: FlatButton) {
        print(isSelected)
        isSelected = !isSelected
    }
}










