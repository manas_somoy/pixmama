//
//  SuggestionTextField.swift
//  PixMama
//
//  Created by UTHABO on 22/12/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import DropDown
import IQKeyboardManagerSwift

class SuggestionTextField: CommonTextField {
    
    fileprivate var dropDown: DropDown!
    
    var suggestions: [String]? {
        didSet {
            tempSuggestions = suggestions
        }
    }
    fileprivate var tempSuggestions: [String]? {
        didSet {
            dropDown.dataSource = tempSuggestions!
        }
    }
    
    var selectedSugestion: String?
    var selectionCompletionBlock: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        
        delegate = self
        self.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        dropDown = DropDown()
        dropDown.anchorView = self
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown?.anchorView?.plainView.bounds.height)! + 10)
        dropDown.selectionAction = { [weak self] (index, item) in
            self?.text = item
            self?.selectedSugestion = item
            self?.selectionCompletionBlock?()
            self?.resignFirstResponder()
        }
        dropDown?.cancelAction = { [unowned self] in
            self.resignFirstResponder()
        }
    }
}

extension SuggestionTextField: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 250
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        let timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(showDropDown), userInfo: nil, repeats: false)
    }
    
    @objc
    fileprivate func  showDropDown() {
        if !(self.text?.isEmpty ?? false) {
            tempSuggestions = suggestions?.filter { $0.contains(self.text!) }
        }
        dropDown.show()
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 10
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        dropDown.hide()
    }
    
    @objc
    func textFieldDidChange(_ textField: UITextField) {
        if !(textField.text?.isEmpty ?? false) {
            tempSuggestions = suggestions?.filter { $0.contains(textField.text!) }
        } else {
            tempSuggestions = suggestions
        }
    }
}
