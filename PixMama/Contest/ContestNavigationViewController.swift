//
//  ContestNavigationViewController.swift
//  Uthabo
//
//  Created by UTHABO on 11/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class ContestNavigationViewController: CommonNavigationController {

    override func prepare() {
        super.prepare()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return self.children.last?.preferredStatusBarStyle ?? .default }

}
