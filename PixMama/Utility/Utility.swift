//
//  Utility.swift
//  PixMama
//
//  Created by UTHABO on 3/11/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import Foundation
import UIKit

class Utility {
    
    //Get Passed Time
    class func contestTimeLeft(EndDate endDate: String) -> String? {
        
        var dateString = ""
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let sDate = Date()
        
        guard let eDate = dateFormatter.date(from: endDate) else {
            fatalError("ERROR: Date conversion failed due to mismatched format.")
        }
        
        let miliDiff: Int64 = Int64(eDate.timeIntervalSince1970 * 1000 - sDate.timeIntervalSince1970 * 1000)
        
        var startDateComponents: DateComponents = Calendar.current.dateComponents([.second, .minute, .hour, .day, .month, .year], from: sDate)
        var endDateComponents: DateComponents = Calendar.current.dateComponents([.second, .minute, .hour, .day, .month, .year], from: eDate)
        
        let yearDiff: Int = endDateComponents.year! - startDateComponents.year!
        let monthDiff: Int = endDateComponents.month! - startDateComponents.month!
        let dayDiff: Int = endDateComponents.day! - startDateComponents.day!
        
        if miliDiff < 0 {
            dateFormatter.dateFormat = "MMM d, yyyy"
            dateString = dateFormatter.string(from: eDate)
        } else {
            if yearDiff > 0 {
                dateFormatter.dateFormat = "MMM d, yyyy"
                dateString = "Ends \(dateFormatter.string(from: eDate))"
            } else if monthDiff > 0 || miliDiff >= Milliseconds.ONE_WEEK {
                dateFormatter.dateFormat = "MMM d"
                dateString = "Ends \(dateFormatter.string(from: eDate))"
            } else {
                var oneOrNot = ""
                var diferenceFromToday: DateComponents = Calendar.current.dateComponents([.second, .minute, .hour, .day], from: sDate, to: eDate)
                if miliDiff < Milliseconds.ONE_MINUTE {
                    dateString = "1 Minute Left"
                } else if miliDiff < Milliseconds.ONE_HOUR {
                    oneOrNot = diferenceFromToday.minute! > 1 ? "Minutes Left" : "Minute Left"
                    dateString = String(format: "%ld %@", diferenceFromToday.minute!, oneOrNot)
                } else if miliDiff < Milliseconds.ONE_DAY {
                    oneOrNot = diferenceFromToday.hour! > 1 ? "Hours Left" : "Hour Left"
                    dateString = String(format: "%ld %@", diferenceFromToday.hour!, oneOrNot)
                } else {
                    oneOrNot = dayDiff > 1 ? "Days Left" : "Day Left"
                    dateString = String(format: "%ld %@", dayDiff, oneOrNot)
                }
            }
        }
        return dateString
    }
}

extension UIView {
    
    func rounded() {
        self.layer.cornerRadius = self.bounds.height/2
    }
}

extension UINavigationItem {
    
    func setTitleWith(title: String, subtitle: String) {
        
        let one = UILabel()
        one.text = title
        one.font = UIFont.systemFont(ofSize: 17)
        one.sizeToFit()
        
        let two = UILabel()
        two.text = subtitle
        two.font = UIFont.systemFont(ofSize: 12)
        two.textAlignment = .center
        two.sizeToFit()
        
        let stackView = UIStackView(arrangedSubviews: [one, two])
        stackView.distribution = .equalCentering
        stackView.axis = .vertical
        
        let width = max(one.frame.size.width, two.frame.size.width)
        stackView.frame = CGRect(x: 0, y: 0, width: width, height: 35)
        
        one.sizeToFit()
        two.sizeToFit()
        
        self.titleView = stackView
    }
}

extension CALayer {
    
    func applySktchShadow(withColorName sColor: String!, alpha sAlpha: Float, width x: CGFloat, height y: CGFloat, blur sBlur: CGFloat, spread sSpread: CGFloat) {
        shadowColor = UIColor.hexStringToUIColor(hexString: sColor).cgColor
        shadowOpacity = sAlpha
        shadowRadius = sBlur / 2.0
        shadowOffset = CGSize(width: x, height: y)
        masksToBounds = false
        var rect: CGRect
        if sSpread == 0 {
            rect = bounds
        } else {
            rect = bounds.insetBy(dx: -sSpread, dy: -sSpread)
        }
        if cornerRadius > 0 {
            shadowPath = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius).cgPath
        } else {
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
    
}

extension UIColor {
    class func hexStringToUIColor (hexString:String) -> UIColor {
        var cString:String = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    class func hexStringToUIColorWithAlpha (hexString: String, alpha: CGFloat) -> UIColor {
        var cString:String = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}
