//
//  ImageContentCell.swift
//  Uthabo
//
//  Created by UTHABO on 30/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import Kingfisher

class ImageContentCell: UICollectionViewCell {


    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var contentImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cornerRadiusPreset = .cornerRadius1
        containerView.cornerRadiusPreset = .cornerRadius1
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.masksToBounds = true
        shadowColor = UIColor.lightGray
        self.depthPreset = .depth2
    }
}
