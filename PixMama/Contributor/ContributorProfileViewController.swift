//
//  ContributorProfileViewController.swift
//  PixMama
//
//  Created by UTHABO on 20/11/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import SwiftyJSON

class ContributorProfileViewController: BaseViewController {

    @IBOutlet weak var profileTableVIew: UITableView!
    var contributor: Contributor?
    
    let pivot = ContributorContentPagination()
    
    fileprivate let itemsPerCall = 25
    fileprivate var contributorContents = [ImageContentData]()
    
    //ApiCallFlag
    var primaryCall = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.titleLabel.text = "PIXMAMA"
        navigationItem.detailLabel.text = "Contributor"

        request?.delegate = self
        
        profileTableVIew.delegate = self
        profileTableVIew.dataSource = self
        profileTableVIew.separatorStyle = .none
        profileTableVIew.backgroundColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.BaseViewColor)
        fetchContributorContents()
    }
    
    func fetchContributorContents() {
        if !primaryCall {
            primaryCall = true
            DispatchQueue.main.async {
                self.request?.fetchContributorContents(ContributorID: (self.contributor?.id)!, GroupID: 1, PageNo: self.pivot.currentPageNo + 1, ItemsCount: self.itemsPerCall)
            }
        }
    }
    func fetchContentDetail(contentId: String) {
        DispatchQueue.main.async {
            self.request?.fetchContentDetail(ContentID: contentId)
        }
    }
    
    func fetchContributorContentsCompleted(DataDictionary dataDictionary: [String : Any]?) {
        if let dic = dataDictionary {
            //            print(dic)
            let json = JSON(dic)
            
            pivot.setPagination(paginationJSON: json["pagination"], itemCount: itemsPerCall)
            let imageData = ImageContentArray(dataArray: json["data"]["contents"].arrayObject as? [[String: Any]])
            
            contributorContents.append(contentsOf: imageData.resultArray)
            
            profileTableVIew.beginUpdates()
            profileTableVIew.reloadRows(at: [IndexPath(row:0, section: 0)], with: .none)
            profileTableVIew.endUpdates()
        }
        primaryCall = false
    }
    
    func fetchContributorContentsFailed(DataDictionary dataDictionary: [String : Any]?) {
        primaryCall = false
        print("fetchContributorContentsFailed ======>>>>>>> \(String(describing: dataDictionary))")
    }
    
    func fetchContentDetailCompleted(DataDictionary dataDictionary: [String : Any]?) {
        if let dic = dataDictionary {
            let json = JSON(dic)
            if let imageVC = storyboard?.instantiateViewController(withIdentifier: "ImageDetailViewController") as? ImageDetailViewController {
                imageVC.imageData = ImageContentData(jsonData: json["data"])
                
                if imageVC.imageData != nil {
                    navigationController?.pushViewController(imageVC, animated: true)
                }
            }
        }
    }
    
    func fetchContentDetailFailed(DataDictionary dataDictionary: [String : Any]?) {
        print("fetchContentDetailFailed ====== >>>>>>>> \(String(describing: dataDictionary))")
        contentNotFound()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if profileTableVIew.contentOffset.y >= (profileTableVIew.contentSize.height - profileTableVIew.frame.size.height) {
            if pivot.has_pivot! {
                fetchContributorContents()
            }
        }
    }
}

extension ContributorProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if contributorContents.count > 0 {
            let itemHeight = PhotoCollectionConstant.itemHeight
            let totalRow = ceil(CGFloat(contributorContents.count) / PhotoCollectionConstant.column)
            let totalTopBottomOffset = PhotoCollectionConstant.offset + PhotoCollectionConstant.offset
            let totalSpacing = CGFloat(totalRow - 1) * PhotoCollectionConstant.minLineSpacing
            let totalHeight  = ((itemHeight * CGFloat(totalRow)) + totalTopBottomOffset + totalSpacing)
            
            return totalHeight + PhotoCollectionConstant.titleLabelHeaderHeight
        } else {
            return UITableView.automaticDimension
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = profileTableVIew.dequeueReusableCell(withIdentifier: "ContributorProfileCell") as? ContributorProfileCell
        
        cell?.parent = self
        cell?.profileImageView.kf.indicatorType = .custom(indicator: ImageIndicator())
        cell?.profileImageView.kf.setImage(with: URL(string: (contributor?.profileImageUrl)!), placeholder: UIImage(named: "iconHomeContributor"), options: nil, progressBlock: nil, completionHandler: nil)
        cell?.nameLabel.text = contributor?.name
        cell?.emailLabel.text = contributor?.email
        cell?.contentCountLabel.text = contributor?.contentCount
        cell?.followerCountLabel.text = contributor?.followersCount
        cell?.followingCountLabel.text = contributor?.followingCount
        
        if contributorContents.count != cell?.contributorContents.count {
            cell?.contributorContents = contributorContents
            cell?.photosCollectionView.reloadData()
        }
        
        return cell!
    }
}

//MARK:- ContributorProfileCell
struct PhotoCollectionConstant {
    static let titleLabelHeaderHeight: CGFloat = 326
    static let column: CGFloat = 3
    
    static let offset: CGFloat = 2
    static let minLineSpacing: CGFloat = 2
    static let minItemSpacing: CGFloat = 2
    
    static func getItemWidth() -> CGFloat{
        
        // totalCellWidth = (collectionview width or tableview width) - (left offset + right offset) - (total space x space width)
        let totalWidth = UIScreen.main.bounds.width - (offset + offset) - ((column - 1) * minItemSpacing)
        
        return totalWidth / column
    }
    
    static let itemHeight: CGFloat = getItemWidth()
}


class ContributorProfileCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var followButton: FlatButton!
    @IBOutlet weak var contentCountLabel: UILabel!
    @IBOutlet weak var followerCountLabel: UILabel!
    @IBOutlet weak var followingCountLabel: UILabel!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    var contributorContents = [ImageContentData]()
    var parent: ContributorProfileViewController?
    var pivot: ContributorContentPagination?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        photosCollectionView.delegate = self
        photosCollectionView.dataSource = self
        photosCollectionView.isScrollEnabled = false
        
        profileImageView.rounded()
        profileImageView.clipsToBounds = true
        profileImageView.borderColor = UIColor.hexStringToUIColor(hexString: "dfdfdf")
        profileImageView.borderWidthPreset = .border2
        
        followButton.backgroundColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.MamaRed)
        followButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
    }
}

extension ContributorProfileCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contributorContents.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContributorContentCell", for: indexPath) as? ContributorContentCell
        
        let singleDetail = contributorContents[indexPath.item]
        
        cell?.contentImageView.kf.indicatorType = .custom(indicator: ImageIndicator())
        cell?.contentImageView.kf.setImage(with: URL(string: singleDetail.imageURL ?? ""))
        
        return cell!
    }
}

extension ContributorProfileCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//            print(contributorContents[indexPath.item])
        let content = contributorContents[indexPath.item]
        parent?.fetchContentDetail(contentId: content.id ?? "")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return EdgeInsets(top: PhotoCollectionConstant.offset, left: PhotoCollectionConstant.offset, bottom: PhotoCollectionConstant.offset, right: PhotoCollectionConstant.offset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return PhotoCollectionConstant.minLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return PhotoCollectionConstant.minItemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = PhotoCollectionConstant.getItemWidth()
        let itemHeight = PhotoCollectionConstant.itemHeight
        
        return CGSize(width: itemWidth, height: itemHeight)
    }
}


//MARK:- ContributorContentCell
class ContributorContentCell: UICollectionViewCell {
    
    @IBOutlet weak var contentImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.cornerRadiusPreset = .cornerRadius1
        contentView.depthPreset = .depth1
        contentView.borderWidthPreset = .border1
        contentView.borderColor = UIColor.hexStringToUIColor(hexString: "dfdfdf")
        contentImageView.clipsToBounds = true
        contentImageView.cornerRadiusPreset = .cornerRadius1
    }
}

//MARK:- ContributorContentPagination
class ContributorContentPagination {
    
    var has_pivot: Bool?
    var total: Int = 0
    var count: Int = 0
    var currentPageNo: Int = 0
    var lastPageNo: Int = 0
    
    init() {
        has_pivot = false
        total = 0
        count = 0
        currentPageNo = 1
        lastPageNo = 1
    }
    
    func resetPagination() {
        has_pivot = false
        total = 0
        count = 0
        currentPageNo = 1
        lastPageNo = 1
    }
    
    func setPagination(paginationJSON: JSON, itemCount: Int) {
        has_pivot = paginationJSON["has_more_pages"].boolValue
        total = paginationJSON["total_items_count"].intValue
        lastPageNo = paginationJSON["last_page_no"].intValue
        currentPageNo = paginationJSON["current_page_no"].intValue
        
        if total > count + itemCount {
            count += itemCount
        } else {
            count = total
        }
    }
}
