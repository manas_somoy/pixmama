//
//  HomeFeatureCollectionViewCell.swift
//  Uthabo
//
//  Created by UTHABO on 3/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class HomeFeatureCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var cellImageView: UIImageView!
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var favoriteImage: UIImageView!
    @IBOutlet weak var favouriteCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        favoriteImage.image = Icon.favorite?.tint(with: Color.pink.darken2)
        containerView.cornerRadiusPreset = .cornerRadius1
        containerView.backgroundColor = Color(ciColor: CIColor(red: 244, green: 242, blue: 235))
    }

    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        let shadowPath = UIBezierPath(rect: bounds)
        layer.masksToBounds = false
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 3
        layer.shadowPath = shadowPath.cgPath
    }
}
