//
//  ImageContentViewController.swift
//  Uthabo
//
//  Created by UTHABO on 30/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import Kingfisher
import SwiftyJSON
import Lottie

enum ContentType: Int {
    case CategoryImages
    case LatestImages
}

class ImageContentViewController: BaseViewController {
    
    fileprivate let cellID = "ImageContentCell"
    fileprivate let itemsPerCall = 15
    
    var groupSlug : String?
    var categorySlug : String?
    var categoryName : String?
    
    var contentArray = [ImageContentData]()
    let pivot = Pagination()
    
    @IBOutlet weak var contentCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationButtons()
        prepareNavigationItem()

        request?.delegate = self
        
        fetchContent()
        
        contentCollectionView.isScrollEnabled = true
        contentCollectionView.delegate = self
        contentCollectionView.dataSource = self
        
        contentCollectionView.register(UINib(nibName: cellID, bundle: nil), forCellWithReuseIdentifier: cellID)
        contentCollectionView.contentInset = EdgeInsets(top: 10, left: 5, bottom: 10, right: 5)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension ImageContentViewController {
    
    fileprivate func prepareNavigationItem() {
        
        navigationItem.titleLabel.text = "\(categoryName ?? "Image")"
        navigationItem.detailLabel.text = "Image Gallery"
    }
    
    fileprivate func prepareNavigationButtons() {
//        navigationItem.backButton.tintColor = Color.grey.darken4
    }
}

//MARK:- Api Call
extension ImageContentViewController {
    func fetchContent() {
        showHUD()
        DispatchQueue.main.async {
            self.request?.fetchContentsForCategory(catergorySlug: self.categorySlug ?? "", groupSlug: self.groupSlug ?? "", pageNo: self.pivot.currentPageNo, numberPerPage: self.itemsPerCall)
        }
    }
}

//MARK:- ApiRequestingDelegate
extension ImageContentViewController {
    
    func fetchContentApiCompleted(DataDictionary dataDictionary: [String : Any]?) {
        if let dic = dataDictionary {
//            print(dic)
            let json = JSON(dic)
            
            pivot.setPagination(paginationJSON: json["data"]["pagination"], itemCount: itemsPerCall)
            let imageData = ImageContentArray(dataArray: json["data"]["data"].arrayObject as? [[String: Any]])
            
            contentArray.append(contentsOf: imageData.resultArray)
            contentCollectionView.reloadData()
        }
        hideHud()
    }
    
    func fetchContentApiFailed(DataDictionary dataDictionary: [String : Any]?) {
        hideHud()
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: AlertConstants.NoDataFound.TITLE, message: AlertConstants.NoDataFound.MESSAGE, preferredStyle: .alert)
            let yesButton = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
                self.navigationController?.popViewController(animated: true)
            })
            alert.addAction(yesButton)
            self.present(alert, animated: true)
        })
    }
}

//MARK:- CollectionViewLayout
extension ImageContentViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return contentArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as? ImageContentCell
        
        let cellContent : ImageContentData = contentArray[indexPath.row]
        
        cell?.contentImageView.kf.indicatorType = .custom(indicator: ImageIndicator())
        cell?.contentImageView.kf.setImage(with: URL(string: cellContent.imageURL ?? ""))

        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let contentVC = storyboard?.instantiateViewController(withIdentifier: "ImageDetailViewController") as? ImageDetailViewController {
            contentVC.imageData = contentArray[indexPath.item]
            navigationController?.pushViewController(contentVC, animated: true)
        }
    }
}

extension ImageContentViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemSize = (contentCollectionView.frame.width - (contentCollectionView.contentInset.left + contentCollectionView.contentInset.right + 5)) / 2
        return CGSize(width: itemSize, height: itemSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

        if indexPath.item == contentArray.count - 4 {
            if let flag = pivot.has_pivot {
                if flag && pivot.total > pivot.count {
                    fetchContent()
                }
            } else {
                print("ImageContent pivot is nil")
            }
        }
    }
}



