//
//  ApiRequest.swift
//  Uthabo
//
//  Created by UTHABO on 12/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

@objc protocol ApiRequestingDelegate {
    @objc optional func internetConnectivityError()
    @objc optional func operationFailed()
    @objc optional func unauthorizedError()
    @objc optional func serverError()
    @objc optional func timeoutError()
    @objc optional func contentNotFound()
    @objc optional func sessionOut()
    
    //User Registration & Authentication Delegates
    @objc optional func registerUserCompleted(DataDictionary dataDictionary: [String: Any]?)
    @objc optional func registerUserFailed(DataDictionary dataDictionary: [String: Any]?)
    @objc optional func signInCompleted(DataDictionary dataDictionary: [String: Any]?)
    @objc optional func signInFailed(DataDictionary dataDictionary: [String: Any]?)
    
    //Home Data Delegate
    @objc optional func getGroupsApiCompleted(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func getGroupsApiFailed(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchLatestImagesCompleted(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchLatestImagesFailed(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchFeaturedContributorsCompleted(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchFeaturedContributorsFailed(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func getDashBoardDataCompleted(DataDictionary dataDictionary: [String: Any]?)
    @objc optional func getDashBoardDataFailed(DataDictionary dataDictionary: [String: Any]?)
    
    //Fetch Content Delegates
    @objc optional func fetchCategoriesApiCompleted(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchCategoriesApiFailed(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchContentApiCompleted(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchContentApiFailed(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchContentDetailCompleted(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchContentDetailFailed(DataDictionary dataDictionary: [String : Any]?)
    
    //Search Content Delegate
    @objc optional func fetchSearchApiCompleted(DataDictionary dataDictionary: [Any]?)
    @objc optional func fetchSearchApiFailed(DataDictionary dataDictionary: [Any]?)
    
    //Fetch Contributor Delegate
    @objc optional func fetchContributorDataCompleted(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchContributorDataFailed(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchContributorContentsCompleted(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchContributorContentsFailed(DataDictionary dataDictionary: [String : Any]?)
    
    //Fetch Contest Delegates
    @objc optional func fetchContestApiCompleted(DataDictionary dataDictionary: [String : Any]?)
    @objc optional func fetchContestApiFailed(DataDictionary dataDictionary: [String : Any]?)
}

enum HTTPResult : Int {
    case HTTP_OK
    case HTTP_CONTENT_NOT_FOUND
    case HTTP_BAD_REQUEST
    case HTTP_UNAUTHORIZE
    case HTTP_SERVER_ERROR
    case HTTP_NG
    case HTTP_TIMEOUT_ERROR = -1001
}

class ApiRequest {
    var delegate: ApiRequestingDelegate!
    fileprivate var appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    //MARK:- Fetch status code
    func httpResult(fromStatusCode responseStatus: Int) -> HTTPResult {
        var result: HTTPResult
        switch responseStatus {
        case 200:
            result = HTTPResult.HTTP_OK
        case 201:
            result = HTTPResult.HTTP_OK
        case 400:
            result = HTTPResult.HTTP_BAD_REQUEST
        case 401:
            result = HTTPResult.HTTP_UNAUTHORIZE
        case 403:
            result = HTTPResult.HTTP_UNAUTHORIZE
        case 404:
            result = HTTPResult.HTTP_CONTENT_NOT_FOUND
        case -1001:
            result = HTTPResult.HTTP_TIMEOUT_ERROR
        case 500, 502, 503:
            result = HTTPResult.HTTP_SERVER_ERROR
        default:
            result = HTTPResult.HTTP_NG
        }
        return result
    }
    
    //MARK:- Show error alert for HTTP error
    func showApiCallFailedAlert(StatusCode code: HTTPResult) {
        switch code {
        case .HTTP_CONTENT_NOT_FOUND:
            delegate.contentNotFound!()
        case .HTTP_BAD_REQUEST:
            delegate.operationFailed!()
        case .HTTP_UNAUTHORIZE:
            delegate.unauthorizedError!()
        case .HTTP_SERVER_ERROR:
            delegate.serverError!()
        case .HTTP_TIMEOUT_ERROR:
            delegate.timeoutError!()
        default:
            delegate.operationFailed!()
        }
    }
    
    //MARK:- Sign up Api
    func performSignUpWith(Email email: String, Password password: String, ConfirmedPassword confirmedPassword: String, FullName fullName: String, UserName userName: String, Country country: String, State state: String, Company company: String) {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["email": email,
                              "password": password,
                              "password_confirmation": confirmedPassword,
                              "full_name": fullName,
                              "user_name": userName,
                              "country": country,
                              "state": state,
                              "company": company]
            
            let headers = ["Content-Type": "application/json"]
            
            let url = "\(LOGIN_BASEURL)\(APIUrls.BUYER_REGISTRATION)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("JSON: \(json)")
                    
                    if json["success"] == true {
                        self.delegate.registerUserCompleted!(DataDictionary: json.dictionaryValue)
                    } else {
                        self.delegate.registerUserFailed!(DataDictionary: json.dictionaryValue)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func fetchCountries(completion: @escaping (JSON) -> Void, failure: @escaping (JSON) -> Void) {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["": ""]
            
            let headers = ["Accept": "application/json"]
            
            let url = "\(BUYER_BASEURL)\(APIUrls.COUNTRY)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("\nfetchCountry: \(json)\n")
                    
                    if json["success"] == true {
                        completion(json)
                    } else {
                        failure(json)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func fetchStates(CountryCode countryCode: String, completion: @escaping (JSON) -> Void, failure: @escaping (JSON) -> Void) {
        
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["country_code": countryCode]
            
            let headers = ["Accept": "application/json"]
            
            let url = "\(BUYER_BASEURL)\(APIUrls.STATE)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("\nfetchStates: \(json)\n")
                    
                    if json["success"] == true {
                        completion(json)
                    } else {
                        failure(json)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }

    //MARK:- Sign in Api
    func performSignInWith(Email email: String, Password password: String) {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["email": email,
                              "password": password]
            
            let headers = ["Content-Type": "application/json"]
            
            let url = "\(LOGIN_BASEURL)\(APIUrls.BUYER_LOGIN)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("JSON: \(json)")
                    
                    if json["success"] == true {
                        self.delegate.signInCompleted!(DataDictionary: json.dictionaryObject)
                    } else {
                        self.delegate.signInFailed!(DataDictionary: json.dictionaryValue)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    //MARK:- Verify Email
    func verifyEmail(Email email: String, Code code: String, completion: @escaping (JSON) -> Void, failure: @escaping (JSON) -> Void) {
        
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["email": email,
                              "token": code]
            
            let headers = ["Accept": "application/json"]
            
            let url = "\(BUYER_BASEURL)\(APIUrls.VERIFY_EMAIL)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("\nerifyEmail: \(json)\n")
                    
                    if json["success"] == true {
                        completion(json)
                    } else {
                        failure(json)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    //MARK:- Forgot Password
    func changePassword(Old oldPassword: String, New newPassword: String, Confirmation conPassword: String, completion: @escaping (JSON) -> Void, failure: @escaping (JSON) -> Void) {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["old_password": oldPassword,
                              "new_password": newPassword,
                              "new_password_confirmation": conPassword]
            
            let headers = ["Accept": "application/json"]
            
            let url = "\(BUYER_BASEURL)\(APIUrls.FORGOT_PASSWORD)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("\nchangePassword: \(json)\n")
                    
                    if json["success"] == true {
                        completion(json)
                    } else {
                        failure(json)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    //MARK:- Home Page Data
    func fetchGroups() {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["": ""]
            
            let headers = ["Content-Type": "application/json"]
            
            let url = "\(CONTENT_BASEURL)\(APIUrls.GET_GROUPS)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    //                    print("fetchGroups \(json)")
                    
                    if json["success"] == true {
                        self.delegate.getGroupsApiCompleted!(DataDictionary: json.dictionaryObject)
                    } else {
                        self.delegate.getGroupsApiCompleted!(DataDictionary: json.dictionaryObject)
                    }
                    
                    
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func fetchLatestImage(With count: Int) {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["total_items": "\(count>0 ? count : 10)"]
            
            let headers = ["Accept": "application/json"]
            
            let url = "\(CONTENT_BASEURL)\(APIUrls.GET_LATEST_IMAGES)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("fetchLatestImage: \(json)")
                    
                    if json["success"].boolValue == true {
                        self.delegate.fetchLatestImagesCompleted!(DataDictionary: json.dictionaryObject)
                    } else {
                        self.delegate.fetchLatestImagesFailed!(DataDictionary: json.dictionaryObject)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func fetchFeaturedContributors() {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["": ""]
            let headers = ["Accept": "application/json"]
            let url = "\(CONTRIBUTOR_BASEURL)\(APIUrls.GET_FEATURED_CONTRIBUTORS)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("fetchFeaturedContributors: \(json)")
                    
                    if json["success"] == true {
                        self.delegate.fetchFeaturedContributorsCompleted!(DataDictionary: json.dictionaryObject)
                    } else {
                        self.delegate.fetchFeaturedContributorsFailed!(DataDictionary: json.dictionaryObject)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func fetchDashBoardData() {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let headers = ["Content-Type": "application/json"]
            
            let url = "\(DASHBOARD_BASEURL)\(APIUrls.TOTAL_DATA)"
            
            
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    //                    print("DASHBOARD DATA:/n\(json)")
                    
                    if json["success"] == true {
                        self.delegate.getDashBoardDataCompleted!(DataDictionary: json.dictionaryObject)
                    } else {
                        self.delegate.getDashBoardDataFailed!(DataDictionary: json.dictionaryObject)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    //MARK:- Fetch Contents
    func fetchCategoriesForGroup(groupSlug: String) {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["slug": groupSlug]
            
            let headers = ["Content-Type": "application/json"]
            
            let url = "\(CONTENT_BASEURL)\(APIUrls.GET_CATEGORIES)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("JSON: \(json)")
                    
                    if json["success"] == true {
                        self.delegate.fetchCategoriesApiCompleted!(DataDictionary: json.dictionaryObject)
                    } else {
                        self.delegate.fetchCategoriesApiFailed!(DataDictionary: json.dictionaryObject)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func fetchContentsForCategory(catergorySlug: String, groupSlug: String, pageNo: Int, numberPerPage: Int) {
        
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["group": groupSlug,
                              "category": catergorySlug,
                              "page": String(pageNo),
                              "per_page": String(numberPerPage)]
            
            let headers = ["Content-Type": "application/json"]
            
            let url = "\(CONTENT_BASEURL)\(APIUrls.GET_CONTENTS)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("JSON: \(json)")
                    
                    if json["success"] == true {
                        self.delegate.fetchContentApiCompleted!(DataDictionary: json.dictionaryObject)
                    } else {
                        self.delegate.fetchContentApiFailed!(DataDictionary: json.dictionaryObject)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func fetchContentDetail(ContentID contentId: String) {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["product_id": contentId]
            
            let headers = ["Accept": "application/json"]
            
            let url = "\(CONTENT_BASEURL)\(APIUrls.GET_CONTENT_DETAIL)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("fetchContentDetail: \(json)\n")
                    
                    if json["success"] == true {
                        self.delegate.fetchContentDetailCompleted!(DataDictionary: json.dictionaryObject)
                    } else {
                        self.delegate.fetchContentDetailFailed!(DataDictionary: json.dictionaryObject)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    //MARK:- Contributor API
    func fetchAllContributors(completion: @escaping (JSON) -> Void, failed: @escaping (JSON) -> Void) {
        
        if NetworkReachabilityManager()!.isReachable == true {
            
            let headers = ["Accept": "application/json"]
            
            let url = "\(CONTRIBUTOR_BASEURL)\(APIUrls.GET_CONTRIBUTOR_LIST)"
            
            Alamofire.request(url, method: .post, parameters: [:], encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("\nfetchAllContributors: \(json)\n")
                    
                    if json["success"] == true {
                        completion(json)
                    } else {
                        failed(json)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func fetchContributorData(ContributorID id: Int) {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["contributor_id": id]
            
            let headers = ["Accept": "application/json"]
            
            let url = "\(CONTRIBUTOR_BASEURL)\(APIUrls.GET_CONTRIBUTOR_DETAILS)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("\nfetchContributorData: \(json)\n")
                    
                    if json["success"] == true {
                        self.delegate.fetchContributorDataCompleted!(DataDictionary: json.dictionaryObject)
                    } else {
                        self.delegate.fetchContributorDataFailed!(DataDictionary: json.dictionaryObject)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func fetchContributorContents(ContributorID id: Int, GroupID groupId: Int, PageNo page: Int, ItemsCount count: Int) {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["contributor_id": id,
                              "group_id": groupId,
                              "page_no": page,
                              "items_count": count]
            
            let headers = ["Accept": "application/json"]
            
            let url = "\(CONTRIBUTOR_BASEURL)\(APIUrls.GET_CONTRIBUTOR_CONTENTS)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("\nfetchContributorContents: \(json)\n")
                    
                    if json["success"] == true {
                        self.delegate.fetchContributorContentsCompleted!(DataDictionary: json.dictionaryObject)
                    } else {
                        self.delegate.fetchContributorContentsFailed!(DataDictionary: json.dictionaryObject)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func followContributor(ContributorID contributorId: Int, completion: @escaping (JSON) -> Void, failed: @escaping (JSON) -> Void) {
        
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["following_id": "\(contributorId)"]
            
            var headers = ["Accept": "application/json"]
            if let token = appDelegate?.fetchBearerToken() {
                if token.count > 0 {
                    headers["Authorization"] = "Bearer \(token)"
                }
            }
            
            let url = "\(BUYER_BASEURL)\(APIUrls.FOLLOW_CONTRIBUTOR)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("\nunfollowContributor: \(json)\n")
                    
                    if json["success"] == true {
                        completion(json)
                    } else {
                        failed(json)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func unfollowContributor(ContributorID contributorId: Int, completion: @escaping (JSON) -> Void, failed: @escaping (JSON) -> Void) {
        
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["following_id": "\(contributorId)"]
            
            var headers = ["Accept": "application/json"]
            if let token = appDelegate?.fetchBearerToken() {
                if token.count > 0 {
                    headers["Authorization"] = "Bearer \(token)"
                }
            }
            
            let url = "\(BUYER_BASEURL)\(APIUrls.FOLLOW_CONTRIBUTOR)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("\nunfollowContributor: \(json)\n")
                    
                    if json["success"] == true {
                        completion(json)
                    } else {
                        failed(json)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    //MARK:- Search API
    func searchContentWith(Query query: String, Group group: String) {
        
        if NetworkReachabilityManager()!.isReachable == true {
            
            let headers = ["Content-Type": "application/json"]
            let url = "\(SEARCH_BASEURL)group=\(group)&query=\(query)"
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("searchContentWith: ======>>>>>> \(json)")
                    
                    self.delegate.fetchSearchApiCompleted!(DataDictionary: json.arrayObject)
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    //MARK:- Order API
    func fetchSubscriptionList(success: @escaping (JSON) -> Void, failure: @escaping (JSON) -> Void) {
        
    }
    
    //MARK:- Contest Api
    func getContests(ByType type: String, PageNo pageNo: Int, ItemsCount itemsPerPage: Int) {
        
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["type": "\(type)",
                              "page_no": String(pageNo),
                              "items_count": String(itemsPerPage)]
            
            let headers = ["Content-Type": "application/json"]
            
            let url = "\(CONTEST_BASEURL)\(APIUrls.GET_CONTEST)"
            
            
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("JSON: \(json)")
                    
                    if json["success"] == true {
                        self.delegate.fetchContestApiCompleted!(DataDictionary: json.dictionaryObject)
                    } else {
                        self.delegate.fetchContestApiFailed!(DataDictionary: json.dictionaryObject)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func fetchContestCategory(completion: @escaping (JSON) -> Void, failed: @escaping (JSON) -> Void) {
        
        if NetworkReachabilityManager()!.isReachable == true {
            
            let headers = ["Accept": "application/json"]
            
            let url = "\(CONTEST_BASEURL)\(APIUrls.GET_CONTEST_CATEGORIES)"
            
            Alamofire.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
//                    print("\nfetchContestCategory: \(json)\n")
                    
                    if json["success"] == true {
                        completion(json)
                    } else {
                        failed(json)
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func createContest(Name name: String, ContestType type: String, CategoryID categoryID: Int, StartDate startDate: String, EndDate endDate: String, Description description: String, Rules rules: String, Tags tags: [String], MaxUpload maxUpload: String, CreatorPointPercentage percentage: String, Banner banner: UIImage, completion: @escaping (JSON) -> Void, failed: @escaping (JSON) -> Void) {
        
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["name": name,
                              "contest_type": type,
                              "contest_category_id": "\(categoryID)",
                              "contest_start": startDate,
                              "contest_end": endDate,
                              "description": description,
                              "rules": rules,
                              "max_upload": maxUpload,
                              "creator_winner_percentage": percentage]
            
            var headers = ["Accept": "application/json"]
            if let token = appDelegate?.fetchBearerToken() {
                if token.count > 0 {
                    headers["Authorization"] = "Bearer \(token)"
                }
            }
            
            let url = "\(APIUrls.CREATE_CONTEST)"
            
            let imageData = banner.jpegData(compressionQuality: 1.0)!
            let tagArray = try? JSONEncoder().encode(tags)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imageData, withName: name,fileName: "\(name).jpg", mimeType: "image/jpg")
                
                //Optional for extra parameters
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
                if let jsonArray = tagArray {
                    multipartFormData.append(jsonArray, withName: "tags")
                }
            }, to:url, method: .post, headers: headers)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    
                    upload.responseJSON { response in
                        switch response.result {
                        case .success(let value):
                            let json = JSON(value)
                            print("\nCreateContest: \(json)\n")
                            
                            if json["success"] == true {
                                completion(json)
                            } else {
                                failed(json)
                            }
                            
                        case .failure(let error):
                            print(error)
                            if let httpStatusCode = response.response?.statusCode {
                                self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                            }
                            if error._code == NSURLErrorTimedOut {
                                print(error._code)
                                self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                            }
                        }
                    }
                    
                case .failure(let encodingError):
                    print(encodingError)
                    break
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
    
    func getUserContests(completion: @escaping (JSON)-> Void) {
        if NetworkReachabilityManager()!.isReachable == true {
            
            let parameters = ["":""]
            
            var headers = ["Accept": "application/json"]
            
            if let token = appDelegate?.fetchBearerToken() {
                if token.count > 0 {
                    headers["Authorization"] = "Bearer \(token)"
                }
            }
            
            let url = "\(CONTEST_BASEURL)\(APIUrls.GET_USER_CONTEST)"
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    print("\ngetUserContests: \(json)\n")
                    
                    if json["success"] == true {
                        
                    } else {
                        
                    }
                    
                case .failure(let error):
                    print(error)
                    if let httpStatusCode = response.response?.statusCode {
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: httpStatusCode))
                    }
                    if error._code == NSURLErrorTimedOut {
                        print(error._code)
                        self.showApiCallFailedAlert(StatusCode: self.httpResult(fromStatusCode: NSURLErrorTimedOut))
                    }
                }
            }
        }
        else{
            delegate.internetConnectivityError!()
        }
    }
}
