//
//  ContributorCollectionViewCell.swift
//  PixMama
//
//  Created by UTHABO on 12/11/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class ContributorCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var contributorImageView: UIImageView!
    @IBOutlet weak var contributorNameLabel: UILabel!
    @IBOutlet weak var followersLabel: UILabel!
    @IBOutlet weak var followButton: FlatButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contributorImageView.rounded()
        contributorImageView.borderColor = UIColor.hexStringToUIColor(hexString: "dfdfdf")
        contributorImageView.borderWidthPreset = .border2
        contributorImageView.clipsToBounds = true
        containerView.borderColor = UIColor.hexStringToUIColor(hexString: "333333")
        containerView.layer.borderWidth = 0.3
        containerView.layer.cornerRadiusPreset = .cornerRadius1
        
        followButton.cornerRadiusPreset = .cornerRadius1
        followButton.backgroundColor = UIColor.hexStringToUIColor(hexString: "bf2c2f")
        followButton.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
    }

}
