//
//  Pagination.swift
//  Uthabo
//
//  Created by UTHABO on 30/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import Foundation
import SwiftyJSON

class Pagination {
    
    var has_pivot: Bool?
    var total: Int = 0
    var count: Int = 0
    var currentPageNo: Int = 0
    var lastPageNo: Int = 0
    
    init() {
        has_pivot = false
        total = 0
        count = 0
        currentPageNo = 1
        lastPageNo = 1
    }
    
    func resetPagination() {
        has_pivot = false
        total = 0
        count = 0
        currentPageNo = 1
        lastPageNo = 1
    }
    
    func setPagination(paginationJSON: JSON, itemCount: Int) {
        has_pivot = paginationJSON["has_more"].boolValue
        total = paginationJSON["total"].intValue
        lastPageNo = paginationJSON["last_page"].intValue
        
        if lastPageNo > currentPageNo {
            currentPageNo += 1
        }
        
        if total > count + itemCount {
            count += itemCount
        } else {
            count = total
        }
    }
}
