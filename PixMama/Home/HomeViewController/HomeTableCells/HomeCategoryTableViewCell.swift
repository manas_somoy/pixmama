//
//  HomeCategoryTableViewCell.swift
//  Uthabo
//
//  Created by UTHABO on 2/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import  Material

class HomeCategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var imageImageView: UIImageView!
    @IBOutlet weak var illustrationImageView: UIImageView!
    
    @IBOutlet weak var imageButton: FlatButton!
    @IBOutlet weak var illustrationButton: FlatButton!
    
    @IBOutlet weak var imageLabel: UILabel!
    @IBOutlet weak var illustrationLabel: UILabel!
    
    fileprivate let buttonColor = "fafafa"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        imageButton.backgroundColor = UIColor.hexStringToUIColor(hexString: buttonColor)
        imageButton.depthPreset = .depth2
        illustrationButton.backgroundColor = UIColor.hexStringToUIColor(hexString: buttonColor)
        illustrationButton.depthPreset = .depth2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
