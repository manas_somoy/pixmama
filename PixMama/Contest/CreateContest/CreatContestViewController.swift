//
//  CreatContestViewController.swift
//  PixMama
//
//  Created by UTHABO on 29/11/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import SwiftyJSON
import DropDown

class CreatContestViewController: BaseViewController, UIScrollViewDelegate {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: CommonScrollView!
    
    @IBOutlet weak var uploadImageView: UIImageView!
    @IBOutlet weak var uploadButton: FlatButton!
    
    @IBOutlet weak var nameTextField: CommonTextField!
    @IBOutlet weak var categoryTextField: CommonTextField!
    @IBOutlet weak var startDateTextField: CommonTextField!
    @IBOutlet weak var endDateTextField: CommonTextField!
    @IBOutlet weak var storylineTextView: UITextView!
    @IBOutlet weak var tagsTextView: TextView!
    
    @IBOutlet weak var contestTypeView: UIView!
    let radioGroup = RadioButtonGroup()
    let publicRadioButton = RadioButton()
    let privateRadioButton = RadioButton()
    
    @IBOutlet weak var uploadLimitTextField: CommonTextField!
    var uploadLimitDropDown: DropDown?
    var categoryDropDown: DropDown?
    var noOfWinnerDropDown: DropDown?
    var ownerMarkDropDown: DropDown?
    
    @IBOutlet weak var winnerNumberTextField: CommonTextField!
    @IBOutlet weak var ownerPercentTextField: CommonTextField!
    @IBOutlet weak var createNewButton: FlatButton!
    
    fileprivate let datePicker = UIDatePicker()
    fileprivate var startDate: Date?
    fileprivate var endDate: Date?
    
    fileprivate var contestCategories = [ContestCategory]()
    fileprivate var selectedCategoryID: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        request?.delegate = self
        
        scrollView.keyboardDismissMode = .onDrag
        scrollView.delegate = self
        
        fetchContestCategory()
        
        prepareNavigationItem()
        prepareButtons()
        prepareRadioButtons()
        
        view.backgroundColor = UIColor.white
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        scrollView.endEditing(true)
        contentView.endEditing(true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        contentView.endEditing(true)
        
    }
    
    //MARK: API Call
    func fetchContestCategory() {
        
        showHUD()
        request?.fetchContestCategory(completion: { (dataDictionary) in
            for item in dataDictionary["data"].arrayValue {
                let category = ContestCategory(jsonData: item)
                self.contestCategories.append(category)
            }
            self.prepareTextViews()
            self.hideHud()
        }, failed: { (dataDictionary) in
            self.contentNotFound()
            self.hideHud()
        })
    }
    
    //MARK: Buttons
    @IBAction func uploadButtonPressed(_ sender: FlatButton) {
        let imagePickerVC = UIImagePickerController()
        imagePickerVC.delegate = self
        
        imagePickerVC.sourceType = .photoLibrary
        self.present(imagePickerVC, animated: true, completion: nil)
    }
}

extension CreatContestViewController {
    
    fileprivate func prepareNavigationItem() {
        
        navigationItem.titleLabel.text = "Create Contest"
        navigationItem.detailLabel.text = "Add Contest Info"
    }
    
    fileprivate func prepareNavigationButtons() {
        
    }
    
    fileprivate func prepareTextViews() {
        
        startDateTextField.delegate = self
        endDateTextField.delegate = self
        
        nameTextField.placeholder = "Name"
        
        categoryTextField.placeholder = "Choose Category"
        let dummyView = UIView(frame: CGRect.zero)
        categoryTextField.delegate = self
        categoryTextField.inputView = dummyView
        categoryDropDown = DropDown()
        categoryDropDown?.anchorView = categoryTextField
        categoryDropDown?.bottomOffset = CGPoint(x: 0, y:(categoryDropDown?.anchorView?.plainView.bounds.height)!)
        categoryDropDown?.dataSource = contestCategories.map { $0.name ?? "" }
        categoryDropDown?.selectionAction = { [weak self] (index, item) in
            self?.categoryTextField.text = item
            self?.selectedCategoryID = self?.contestCategories[index].id
            self?.categoryTextField.resignFirstResponder()
        }
        categoryDropDown?.cancelAction = { [unowned self] in
            self.categoryTextField.resignFirstResponder()
        }
        
        startDateTextField.placeholder = "Start Date"
        startDateTextField.errorLabel.fontSize = 11
        startDateTextField.errorColor = Color.grey.darken3
        startDateTextField.errorVerticalOffset = 2
        startDateTextField.isErrorRevealed = true
        startDateTextField.error = "** Must be greater than now!"
        
        endDateTextField.placeholder = "End Date"
        endDateTextField.errorLabel.fontSize = 11
        endDateTextField.errorColor = Color.grey.darken3
        endDateTextField.errorVerticalOffset = 2
        endDateTextField.isErrorRevealed = true
        endDateTextField.error = "** Must be greater than Start Date!"
        
        uploadLimitTextField.delegate = self
        uploadLimitTextField.inputView = dummyView
        uploadLimitTextField.placeholder = "Upload Limit Per Participant"
        uploadLimitDropDown = DropDown()
        uploadLimitDropDown?.anchorView = uploadLimitTextField
        uploadLimitDropDown?.bottomOffset = CGPoint(x: 0, y:(uploadLimitDropDown?.anchorView?.plainView.bounds.height)!)
        uploadLimitDropDown?.dataSource = ["1","2","3","4","5"]
        uploadLimitDropDown?.selectionAction = { [weak self] (index, item) in
            self?.uploadLimitTextField.text = item
            self?.uploadLimitTextField.resignFirstResponder()
        }
        uploadLimitDropDown?.cancelAction = { [unowned self] in
            self.uploadLimitTextField.resignFirstResponder()
        }
        
        winnerNumberTextField.delegate = self
        winnerNumberTextField.inputView = dummyView
        winnerNumberTextField.placeholder = "No of Winners"
        noOfWinnerDropDown = DropDown()
        noOfWinnerDropDown?.anchorView = winnerNumberTextField
        noOfWinnerDropDown?.bottomOffset = CGPoint(x: 0, y:(noOfWinnerDropDown?.anchorView?.plainView.bounds.height)!)
        noOfWinnerDropDown?.dataSource = ["1","2","3","4","5"]
        noOfWinnerDropDown?.selectionAction = { [weak self] (index, item) in
            self?.winnerNumberTextField.text = item
            self?.winnerNumberTextField.resignFirstResponder()
        }
        noOfWinnerDropDown?.cancelAction = { [unowned self] in
            self.winnerNumberTextField.resignFirstResponder()
        }
        
        ownerPercentTextField.delegate = self
        ownerPercentTextField.inputView = dummyView
        ownerPercentTextField.placeholder = "Owners Marks in Percentage"
        ownerMarkDropDown = DropDown()
        ownerMarkDropDown?.anchorView = ownerPercentTextField
        ownerMarkDropDown?.bottomOffset = CGPoint(x: 0, y:(ownerMarkDropDown?.anchorView?.plainView.bounds.height)!)
        ownerMarkDropDown?.dataSource = ["10","20","30","40","50"]
        ownerMarkDropDown?.selectionAction = { [weak self] (index, item) in
            self?.ownerPercentTextField.text = item
            self?.ownerPercentTextField.resignFirstResponder()
        }
        ownerMarkDropDown?.cancelAction = { [unowned self] in
            self.ownerPercentTextField.resignFirstResponder()
        }
        
    }
    
    fileprivate func prepareRadioButtons() {
        
        publicRadioButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 14)
        publicRadioButton.titleColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.MotoTextColor)
        publicRadioButton.titleLabel?.numberOfLines = 0
        publicRadioButton.titleLabel?.lineBreakMode = .byWordWrapping
        publicRadioButton.title = "Public"
        publicRadioButton.setIconColor(UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.PinkColor), for: .selected)
        
        privateRadioButton.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 14)
        privateRadioButton.titleColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.MotoTextColor)
        privateRadioButton.titleLabel?.numberOfLines = 0
        privateRadioButton.titleLabel?.lineBreakMode = .byWordWrapping
        privateRadioButton.title = "Private"
        privateRadioButton.setIconColor(UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.PinkColor), for: .selected)
        
        radioGroup.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        radioGroup.buttons = [publicRadioButton, privateRadioButton]
        contestTypeView.addSubview(radioGroup)
        contestTypeView.layout(radioGroup).topBottom().leadingTrailing()
        
    }
    
    fileprivate func prepareButtons() {
        
        uploadButton.setTitleColor(UIColor.white, for: .normal)
        uploadButton.backgroundColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.FollowBlue)
        uploadButton.cornerRadiusPreset = .cornerRadius2
        uploadButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        
        createNewButton.setTitleColor(UIColor.white, for: .normal)
        createNewButton.backgroundColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.MamaRed)
        createNewButton.cornerRadiusPreset = .cornerRadius2
        createNewButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
    }
}

extension CreatContestViewController: UITextFieldDelegate, TextViewDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == startDateTextField || textField == endDateTextField {
            showDatePicker()
        } else if textField == uploadLimitTextField {
            uploadLimitDropDown?.show()
        } else if textField == winnerNumberTextField {
            noOfWinnerDropDown?.show()
        } else if textField == ownerPercentTextField {
            ownerMarkDropDown?.show()
        } else if textField == categoryTextField {
            categoryDropDown?.show()
        }
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .dateAndTime
        
        if startDateTextField.isFirstResponder {
            datePicker.minimumDate = Date()
        } else if endDateTextField.isFirstResponder {
            if startDate == nil {
                datePicker.minimumDate = Date()
            } else {
                datePicker.minimumDate = startDate
            }
        }
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(donedatePicker(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(cancelDatePicker(_:)))
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        // add toolbar to textField
        startDateTextField.inputAccessoryView = toolbar
        endDateTextField.inputAccessoryView = toolbar
        // add datepicker to textField
        startDateTextField.inputView = datePicker
        endDateTextField.inputView = datePicker
    }
    
    @objc
    func donedatePicker(_ sender: UIBarButtonItem){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        
        if startDateTextField.isFirstResponder {
            startDate = datePicker.date
            startDateTextField.text = formatter.string(from: datePicker.date)
            startDateTextField.resignFirstResponder()
        } else if endDateTextField.isFirstResponder {
            endDate = datePicker.date
            endDateTextField.text = formatter.string(from: datePicker.date)
            endDateTextField.resignFirstResponder()
        }
    }
    
    @objc
    func cancelDatePicker(_ sender: UIBarButtonItem){
        //cancel button dismiss datepicker dialog
        if startDateTextField.isFirstResponder {
            startDateTextField.resignFirstResponder()
        }
        if endDateTextField.isFirstResponder {
            endDateTextField.resignFirstResponder()
        }
    }
}

extension CreatContestViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        uploadImageView.image = image
        
        picker.dismiss(animated: true, completion: nil)
    }
}


//MARK:- Contest Categories Model
class ContestCategory {
    
    var id: Int?
    var name: String?
    var slug: String?
    var imageUrl: String?
    var iconUrl: String?
    var subCategory: [ContestCategory]?
    
    init(jsonData: JSON) {
        id = jsonData["id"].intValue
        name = jsonData["name"].stringValue
        slug = jsonData["slug"].stringValue
        imageUrl = jsonData["image"].stringValue
        iconUrl = jsonData["icon"].stringValue
        
        if jsonData["sub_category"].arrayValue.count > 0 {
            for item in jsonData["sub_category"].arrayValue {
                let subCat = ContestCategory(jsonData: item)
                subCategory?.append(subCat)
            }
        }
    }
}
