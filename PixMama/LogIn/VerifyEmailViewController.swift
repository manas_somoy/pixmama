//
//  VerifyEmailViewController.swift
//  PixMama
//
//  Created by UTHABO on 22/12/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class VerifyEmailViewController: BaseViewController {

    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var emailTextField: CommonTextField!
    @IBOutlet weak var codeTextField: CommonTextField!
    @IBOutlet weak var verifyButton: FlatButton!
    
    var fromInitialView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        request?.delegate = self

        prepareNavigationItem()
        prepareButtons()
        prepareTextViews()
    }

    @IBAction func verifyButtonPressed(_ sender: FlatButton) {
        
        if emailTextField.isEmpty {
            emailTextField.isErrorRevealed = true
            return
        } else {
            emailTextField.isErrorRevealed = false
        }
        
        if codeTextField.isEmpty {
            codeTextField.isErrorRevealed = true
            return
        } else {
            codeTextField.isErrorRevealed = false
        }

        performVerification()
    }
    
    //MARK:- API Call
    fileprivate func performVerification() {
        
        showHUD()
        request?.verifyEmail(Email: emailTextField.text!, Code: emailTextField.text!, completion: { (dataDictionary) in
            
            let alert = UIAlertController.init(title: "Verified!", message: "You are now a member of Pixmama Community.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: { (ok) in
                if let signIn = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 2] {
                    self.navigationController?.popToViewController(signIn, animated: true)
                }
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            self.hideHud()
            
        }, failure: { (dataDictionary) in
            
            var message = ""
            if dataDictionary["errors"].dictionaryObject != nil {
                message += dataDictionary["errors"]["email"].arrayValue[0].stringValue
                if message.count > 0 {
                    message += dataDictionary["errors"]["token"].arrayValue[0].stringValue
                }
            }
            
            let alert = UIAlertController.init(title: dataDictionary["message"].stringValue, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            self.hideHud()
        })
    }
}

extension VerifyEmailViewController {
    
    fileprivate func prepareNavigationItem() {
        if !fromInitialView {
            navigationItem.titleLabel.text = "PIXMAMA"
            navigationItem.detailLabel.text = "Email Verification"
            menuButton.isHidden = true
        } else {
            navigationItem.title = "Verify"
        }
    }
    
    
    
    fileprivate func prepareTextViews() {
        emailTextField.placeholder = "Email"
        emailTextField.error = "Email field must not be empty!!*"
        codeTextField.placeholder = "Enter Verification Code"
        codeTextField.error = "Enter the code that we sent you."
    }
    
    fileprivate func prepareButtons() {
        verifyButton.setTitleColor(UIColor.white, for: .normal)
        verifyButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        verifyButton.backgroundColor = Color.red.base
        verifyButton.cornerRadiusPreset = .cornerRadius2
    }
}
