//
//  SignUpViewController.swift
//  Uthabo
//
//  Created by UTHABO on 25/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import SwiftyJSON
import DropDown
import IQKeyboardManagerSwift

class SignUpViewController: BaseViewController {
    @IBOutlet weak var fullNameTextField: CommonTextField!
    @IBOutlet weak var userNameTextField: CommonTextField!
    @IBOutlet weak var emailTextField: CommonTextField!
    @IBOutlet weak var passwordTextField: PasswordField!
    @IBOutlet weak var confirmPasswordTextField: PasswordField!
    @IBOutlet weak var countryTextField: SuggestionTextField!
    @IBOutlet weak var stateTextField: SuggestionTextField!
    @IBOutlet weak var companyTextField: CommonTextField!
    @IBOutlet weak var registerButton: FlatButton!
    @IBOutlet weak var skipButton: FlatButton!
    
    var countryDropDown: DropDown?
    var stateDropDown: DropDown?
    
    var fromInitialView = false
    
    //Variables
    fileprivate var countries: [String]? {
        didSet {
            countryTextField.suggestions = countries
        }
    }
    fileprivate var countryCodes: [String]!
    
    fileprivate var states: [String]? {
        didSet {
            stateTextField.suggestions = states
        }
    }
    fileprivate var stateCodes: [String]!
    
    fileprivate var selectedCountryCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareNavigationItem()
        prepareTextViews()
        prepareButton()
        
        request?.delegate = self
        fetchCoutries()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func registerButtonPressed(_ sender: FlatButton) {
        DispatchQueue.main.async {
            self.request?.performSignUpWith(Email: self.emailTextField.text ?? "", Password: self.passwordTextField.text ?? "", ConfirmedPassword: self.passwordTextField.text ?? "", FullName: self.fullNameTextField.text ?? "", UserName: self.userNameTextField.text ?? "", Country: self.countryTextField.text ?? "", State: self.stateTextField.text ?? "", Company: self.companyTextField.text ?? "")
        }
    }
    
    @IBAction func skipButtonPressed(_ sender: FlatButton) {
        if fromInitialView {
            delegate?.SetupTabBarController()
            delegate?.changeRootToTabVC()
        } else {
            if let navVCArray = navigationController?.viewControllers {
                navigationController?.popToViewController(navVCArray[navVCArray.count - 3], animated: true)
            }
        }
    }

    //API Call
    fileprivate func fetchCoutries() {
        showHUD()
        request?.fetchCountries(completion: { (dataDictionary) in
            self.countries = dataDictionary["data"]["countries"].arrayValue.map { $0["name"].stringValue }
            self.countryCodes = dataDictionary["data"]["countries"].arrayValue.map { $0["code"].stringValue }
            self.hideHud()
        }, failure: { (dataDictionary) in
            print("\(dataDictionary["message"])")
            self.hideHud()
        })
    }
    
    fileprivate func fetchStates() {
        showHUD()
        request?.fetchStates(CountryCode: selectedCountryCode, completion: { (dataDictionary) in
            self.states = dataDictionary["data"]["states"].arrayValue.map { $0["name"].stringValue }
            self.stateCodes = dataDictionary["data"]["states"].arrayValue.map { $0["code"].stringValue }
            self.hideHud()
        }, failure: { (dataDictionary) in
            print("\(dataDictionary["message"])")
            self.hideHud()
        })
    }
}

extension SignUpViewController {
    
    fileprivate func prepareNavigationItem() {
        if !fromInitialView {
            navigationItem.titleLabel.text = "PIXMAMA"
            navigationItem.detailLabel.text = "Registration"
            menuButton.isHidden = true
        } else {
            navigationItem.title = "Sign Up"
        }
    }
    
    fileprivate func prepareTextViews() {
        emailTextField.placeholder = "Enter Email"
        passwordTextField.placeholder = "Password"
        confirmPasswordTextField.placeholder = "Confirm Password"
        fullNameTextField.placeholder = "Enter Your Full Name"
        userNameTextField.placeholder = "Username For Your Account"
        
        countryTextField.placeholder = "Country"
        countryTextField.selectionCompletionBlock = {
            
            if let selectedItem = self.countryTextField.selectedSugestion {
                self.selectedCountryCode = self.countryCodes[(self.countries?.firstIndex(of: selectedItem))!]
            }
            
            self.fetchStates()
        }
        
        stateTextField.placeholder = "State(Optional)"
        companyTextField.placeholder = "Company Name(Optional)"
    }
    
    fileprivate func prepareButton() {
        registerButton.setTitleColor(UIColor.white, for: .normal)
        registerButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        registerButton.backgroundColor = Color.red.base
        registerButton.cornerRadiusPreset = .cornerRadius2
        
        skipButton.setTitleColor(UIColor.white, for: .normal)
        skipButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        skipButton.backgroundColor = Color.deepPurple.darken1
        skipButton.cornerRadiusPreset = .cornerRadius2
    }
}

extension SignUpViewController {
    
    func registerUserCompleted(DataDictionary dataDictionary: [String : Any]?) {
        print("Registration Complete!!! ======> \(dataDictionary!)")
        navigationController?.popViewController(animated: true)
    }
    
    func registerUserFailed(DataDictionary dataDictionary: [String : Any]?) {
        let data = JSON(dataDictionary!)
        let alert = UIAlertController(title: "Ragistration Failed!", message: data["message"].stringValue, preferredStyle: .alert)
        let tryButton = UIAlertAction(title: "Try Again", style: .default, handler: nil)
        alert.addAction(tryButton)
        present(alert, animated: true, completion: nil)
    }
}
