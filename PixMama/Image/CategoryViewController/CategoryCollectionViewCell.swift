//
//  CategoryCollectionViewCell.swift
//  Uthabo
//
//  Created by UTHABO on 16/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var containerButton: FlatButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        categoryTitleLabel.backgroundColor = UIColor.hexStringToUIColorWithAlpha(hexString: "000000", alpha: 0.6)
        
        self.layer.cornerRadius = 1
        self.clipsToBounds = true
    }

}
