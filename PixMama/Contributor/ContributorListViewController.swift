//
//  ContributorListViewController.swift
//  PixMama
//
//  Created by UTHABO on 9/12/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class ContributorListViewController: BaseViewController {
    
    //variables
    fileprivate var contributors = [Contributor]()

    @IBOutlet weak var contributorTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.titleLabel.text = "Contributors"
        navigationItem.detailLabel.text = "Follow Who You Like"
        
        request?.delegate = self
        
        contributorTableView.delegate = self
        contributorTableView.dataSource = self
        contributorTableView.separatorStyle = .none
        contributorTableView.backgroundColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.BaseViewColor)
        
        fetchAllContributors()
    }
    
    //MARK:- API Call
    fileprivate func fetchAllContributors() {
        showHUD()
        request?.fetchAllContributors(completion: { (dataDictionary) in
            
            if let contributors = dataDictionary["data"]["contributors"].arrayObject as? [[String: Any]] {
                self.contributors.append(contentsOf: ContributorArray(dataArray: contributors).resultArray)
                self.contributorTableView.reloadData()
            }
            self.hideHud()
            
        }, failed: { (dataDictionary) in
            self.contentNotFound()
            self.hideHud()
        })
    }
}

extension ContributorListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contributors.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContributorCell") as? ContributorCell
        
        let singleDetail = contributors[indexPath.row]
        
        cell?.contributorImageView.kf.setImage(with: URL(string: singleDetail.profileImageUrl), placeholder: UIImage(named: "iconHomeContributor"), options: nil, progressBlock: nil, completionHandler: nil)
        cell?.nameLabel.text = singleDetail.name
        cell?.contentCountLabel.text = singleDetail.contentCount + " Uploads"
        
        cell?.followButton.tag = indexPath.row
        cell?.followButton.addTarget(self, action: #selector(followPressed(_:)), for: .touchUpInside)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let contributorVC = storyboard?.instantiateViewController(withIdentifier: "ContributorProfileViewController") as? ContributorProfileViewController {
            contributorVC.contributor = contributors[indexPath.item]
            navigationController?.pushViewController(contributorVC, animated: true)
        }
    }
    
    @objc
    fileprivate func followPressed(_ sender: FlatButton) {
        print("Follow pressed.")
    }
}

//MARK:- ContributorCell
class ContributorCell: TableViewCell {
    
    @IBOutlet weak var contributorImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentCountLabel: UILabel!
    @IBOutlet weak var followButton: FlatButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contributorImageView.rounded()
        contributorImageView.clipsToBounds = true
        contributorImageView.borderColor = UIColor.hexStringToUIColor(hexString: "dfdfdf")
        contributorImageView.borderWidthPreset = .border2
        contributorImageView.backgroundColor = UIColor.hexStringToUIColorWithAlpha(hexString: ColorConstants.AppColor.BaseViewColor, alpha: 0.5)
        dividerColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.BaseViewColor)
        dividerThickness = 1
        
        followButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        followButton.backgroundColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.MamaRed)
    }
}
