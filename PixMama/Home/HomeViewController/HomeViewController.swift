//
//  HomeViewController.swift
//  Uthabo
//
//  Created by UTHABO on 2/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import SwiftyJSON
import Lottie
import Kingfisher

enum HomeTableCellType: Int {
    case ContentGroupCell
    case FeaturedPhotosCell
    case FeaturedContributorsCell
    case ContentStateCell
}

class HomeViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    fileprivate let categoryVCID = "CategoryViewController"
    
    @IBOutlet weak var homeTableView: UITableView!
    
    //HomeCategoryCell Variables
    fileprivate var groupData : [[String : Any]]?
    fileprivate var groups = [Group]()
    fileprivate var categoryArray = [String]()
    fileprivate var slugArray = [String]()
    
    //Featured Data Variables
    fileprivate var featuredPhotos = [ImageContentData]()
    fileprivate var topContributors : [[String : Any]]?
    
    //Stat cell at the end
    fileprivate var dashBoardData: JSON?
    
    let homeDispatch = DispatchGroup()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationButtons()
        prepareNavigationItem()
        
        request?.delegate = self
        
        homeTableView.allowsSelection = false
        homeTableView.separatorStyle = .none
        homeTableView.isHidden = false
        homeTableView.backgroundColor = UIColor.clear
        homeTableView.delegate = self
        homeTableView.dataSource = self
        
        fetchGroups()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchLatestImages()
        fetchTopContributors()
        fetchDashBoardData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: API Request
    func fetchGroups() {
        DispatchQueue.main.async {
            self.request?.fetchGroups()
        }
    }
    
    func fetchLatestImages() {
        DispatchQueue.main.async {
            self.request?.fetchLatestImage(With: 0)
        }
    }
    
    func fetchTopContributors() {
        DispatchQueue.main.async {
            self.request?.fetchFeaturedContributors()
        }
    }
    
    func fetchDashBoardData() {
        DispatchQueue.main.async {
            self.request?.fetchDashBoardData()
        }
    }
    
    //MARK: APIRequestingDelegate
    func getDashBoardDataCompleted(DataDictionary dataDictionary: [String : Any]?) {
        
        if let dic = dataDictionary {
            dashBoardData = JSON(dic["data"] as! [String: Any])
            
            homeTableView.beginUpdates()
            homeTableView.reloadRows(at: [IndexPath(row: HomeTableCellType.ContentStateCell.rawValue, section: 0)], with: .none)
            homeTableView.endUpdates()
        }
    }
    
    func getDashBoardDataFailed(DataDictionary dataDictionary: [String : Any]?) {
        print("getDashBoardDataFailed ========>>>>>>>>\(String(describing: dataDictionary))")
    }
    
    func getGroupsApiCompleted(DataDictionary dataDictionary: [String : Any]?) {
        let data = dataDictionary!["data"] as? [String : Any]
        groupData = data!["groups"] as? [[String: Any]]
        
        UserDefaults.standard.set(self.groupData, forKey: UserConstants.UserDefaultKey.Groups)
        
        homeTableView.beginUpdates()
        homeTableView.reloadRows(at: [IndexPath(row: HomeTableCellType.ContentGroupCell.rawValue, section: 0)], with: .none)
        homeTableView.endUpdates()
    }
    
    func getGroupsApiFailed(DataDictionary dataDictionary: [String : Any]?) {
        print("getGroupsApiFailed =====>>>>> \(String(describing: dataDictionary))")
    }
    
    func fetchLatestImagesCompleted(DataDictionary dataDictionary: [String : Any]?) {
        if let dic = dataDictionary {
            let json = JSON(dic)
            
            let imageData = ImageContentArray(dataArray: json["data"].arrayObject as? [[String: Any]])
            featuredPhotos.append(contentsOf: imageData.resultArray)
            
            homeTableView.beginUpdates()
            homeTableView.reloadRows(at: [IndexPath(row: HomeTableCellType.FeaturedPhotosCell.rawValue, section: 0)], with: .none)
            homeTableView.endUpdates()
        }
    }
    
    func fetchLatestImagesFailed(DataDictionary dataDictionary: [String : Any]?) {
        print("fetchLatestImagesFailed ======>>>>> \(String(describing: dataDictionary))")
    }
    
    func fetchFeaturedContributorsCompleted(DataDictionary dataDictionary: [String : Any]?) {
        if let dic = dataDictionary {
            topContributors = dic["featured_users"] as? [[String: Any]]
            
            homeTableView.beginUpdates()
            homeTableView.reloadRows(at: [IndexPath(row: HomeTableCellType.FeaturedContributorsCell.rawValue, section: 0)], with: .none)
            homeTableView.endUpdates()
        }
    }
    
    func fetchFeaturedContributorsFailed(DataDictionary dataDictionary: [String : Any]?) {
        print("fetchFeaturedContributorsFailed =======>>>>> \(String(describing: dataDictionary))")
    }
    
    //MARK:- TableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case HomeTableCellType.ContentGroupCell.rawValue:     // header cell
            let buttonWidth = UIScreen.main.bounds.width - 30
            let buttonHeight = buttonWidth/2 + 60
            
            return buttonHeight
        case HomeTableCellType.FeaturedPhotosCell.rawValue:     // collection cell
            let itemHeight = FeaturedPhotoCollectionConstant.itemHeight
            let totalRow = ceil(FeaturedPhotoCollectionConstant.totalItem / FeaturedPhotoCollectionConstant.column)
            let totalTopBottomOffset = FeaturedPhotoCollectionConstant.offset * 2 + FeaturedPhotoCollectionConstant.offset * 2
            let totalSpacing = CGFloat(totalRow - 1) * FeaturedPhotoCollectionConstant.minLineSpacing
            let totalHeight  = ((itemHeight * CGFloat(totalRow)) + totalTopBottomOffset + totalSpacing)
            
            return totalHeight + FeaturedPhotoCollectionConstant.titleLabelHeaderHeight
        case HomeTableCellType.FeaturedContributorsCell.rawValue:     //contributor cell
            let itemHeight = TopContributorsConstant.itemHeight + TopContributorsConstant.offset*2
            let totalHeight = itemHeight + TopContributorsConstant.titleLabelHeaderHeight
            
            return totalHeight
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        print(cell.bounds.height)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case HomeTableCellType.ContentGroupCell.rawValue:
            var cell: HomeCategoryTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "HomeCategoryTableViewCell") as? HomeCategoryTableViewCell
            
            if cell == nil {
                let nibName = UINib.init(nibName: "HomeCategoryTableViewCell", bundle: nil)
                
                // register nib
                tableView.register(nibName, forCellReuseIdentifier: "HomeCategoryTableViewCell")
                cell  = tableView.dequeueReusableCell(withIdentifier: "HomeCategoryTableViewCell") as? HomeCategoryTableViewCell
                
            }
            
            if let data = groupData {
                for item : [String : Any] in data {
                    if let id = item["id"] as? Int {
                        if id == 1 {
                            cell?.imageLabel.text = item["name"] as? String
                            cell?.imageButton.tag = (data as NSArray).index(of: item)
                            if let slug = item["slug"] as? String {
                                slugArray.append(slug)
                            }
                            if let name = item["name"] as? String {
                                categoryArray.append(name)
                            }
                            
                            if let url = item["image_url"] as? String {
                                cell?.imageImageView.kf.indicatorType = .custom(indicator: ImageIndicator())
                                cell?.imageImageView.kf.setImage(with: URL(string: url), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                            }
                            
                            cell?.imageButton.addTarget(self, action: #selector(groupButtonTapped(_:)), for: .touchUpInside)
                        } else if id == 4 {
                            cell?.illustrationLabel.text = item["name"] as? String
                            cell?.illustrationButton.tag = (data as NSArray).index(of: item)
                            if let slug = item["slug"] as? String {
                                slugArray.append(slug)
                            }
                            if let name = item["name"] as? String {
                                categoryArray.append(name)
                            }
                            
                            if let url = item["image_url"] as? String {
                                cell?.illustrationImageView.kf.indicatorType = .custom(indicator: ImageIndicator())
                                cell?.illustrationImageView.kf.setImage(with: URL(string: url), placeholder: nil, options: nil, progressBlock: nil, completionHandler: nil)
                            }
                            
                            cell?.illustrationButton.addTarget(self, action: #selector(groupButtonTapped(_:)), for: .touchUpInside)
                        } //else if id == 2 {
//                            cell?.audioLabel.text = item["name"] as? String
//                            cell?.audioButton.tag = id - 1
//                            if let slug = item["slug"] as? String {
//                                slugArray.append(slug)
//                            }
//                            if let name = item["name"] as? String {
//                                categoryArray.append(name)
//                            }
//                            cell?.audioButton.addTarget(self, action: #selector(groupButtonTapped(_:)), for: .touchUpInside)
//                        } else if id == 3 {
//                            cell?.videoLabel.text = item["name"] as? String
//                            cell?.videoButton.tag = id - 1
//                            if let slug = item["slug"] as? String {
//                                slugArray.append(slug)
//                            }
//                            if let name = item["name"] as? String {
//                                categoryArray.append(name)
//                            }
//                            cell?.videoButton.addTarget(self, action: #selector(groupButtonTapped(_:)), for: .touchUpInside)
//                        }
                    }
                }
            }
            
            return cell!
            
        case HomeTableCellType.FeaturedPhotosCell.rawValue:
            var cell : HomeFeatureTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "HomeFeatureTableViewCell") as? HomeFeatureTableViewCell
            
            if cell == nil {
                let nibName = UINib(nibName: "HomeFeatureTableViewCell", bundle: nil)
                
                tableView.register(nibName, forCellReuseIdentifier: "HomeFeatureTableViewCell")
                cell = tableView.dequeueReusableCell(withIdentifier: "HomeFeatureTableViewCell") as? HomeFeatureTableViewCell
            }
            
            cell?.parent = self
            cell?.featuredTitle.text = "Featured Photos"
            if let count = cell?.contentArray.count, count > 0 {
                cell?.contentArray.removeAll()
            }
            cell?.contentArray.append(contentsOf: featuredPhotos)
            cell?.featuredCollectionView.reloadData()
            
            return cell!
            
        case HomeTableCellType.FeaturedContributorsCell.rawValue:
            var cell = tableView.dequeueReusableCell(withIdentifier: "FeaturedContributorsTableViewCell") as? FeaturedContributorsTableViewCell
            
            if cell == nil {
                let nibName = UINib(nibName: "FeaturedContributorsTableViewCell", bundle: nil)
                
                tableView.register(nibName, forCellReuseIdentifier: "FeaturedContributorsTableViewCell")
                cell = tableView.dequeueReusableCell(withIdentifier: "FeaturedContributorsTableViewCell") as? FeaturedContributorsTableViewCell
            }
            
            cell?.parent = self
            
            if let count = cell?.contributors.count, count > 0 {
                cell?.contributors.removeAll()
            }
            
            if let contributors = topContributors {
                let contributorArr = ContributorArray(dataArray: contributors)
                cell?.contributors.append(contentsOf: contributorArr.resultArray)
                cell?.contributorsCollectionView.reloadData()
            }
            
            cell?.seeMoreButton.addTarget(self, action: #selector(contributorSeeMorePressed(_:)), for: .touchUpInside)
            
            return cell!
            
        case HomeTableCellType.ContentStateCell.rawValue:
            var cell = tableView.dequeueReusableCell(withIdentifier: "DashDataTableViewCell") as? DashDataTableViewCell
            
            if cell == nil {
                tableView.register(UINib.init(nibName: "DashDataTableViewCell", bundle: nil), forCellReuseIdentifier: "DashDataTableViewCell")
                cell = tableView.dequeueReusableCell(withIdentifier: "DashDataTableViewCell") as? DashDataTableViewCell
            }
            
            if let dashData = dashBoardData {
                cell?.totalContentsLabel.text = dashData["total_contents"].stringValue
                cell?.totalContestsLabel.text = dashData["total_contests"].stringValue
                cell?.totalContributorsLabel.text = dashData["total_contributors"].stringValue
            }
            
            return cell!
            
        default:
            return UITableViewCell(style: .default, reuseIdentifier: "cell")
        }
        
    }
    
    @objc
    fileprivate func contributorSeeMorePressed(_ sender: UIButton) {
        if let allContributorVc = storyboard?.instantiateViewController(withIdentifier: "ContributorListViewController") as? ContributorListViewController {
            navigationController?.pushViewController(allContributorVc, animated: true)
        }
    }
}
extension HomeViewController {
    
    fileprivate func prepareNavigationItem() {
        
        navigationItem.titleLabel.text = "PIXMAMA"
        navigationItem.detailLabel.text = "Media Community"
    }
    
    fileprivate func prepareNavigationButtons() {
        
    }
    
}

extension HomeViewController {
    
    //MARK:- Button press action
    @objc func groupButtonTapped(_ button: FlatButton?) {
        let catVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: categoryVCID) as? CategoryViewController
        if let id = button?.tag {
            catVC?.slug = slugArray[id]
            catVC?.categoryName = categoryArray[id]
        }
        navigationController?.pushViewController(catVC!, animated: true)
    }
}

struct ImageIndicator: Indicator {
    let view: UIView = UIView()
    let lottie = LOTAnimationView(name: "mamaLoader")
    
    func startAnimatingView() {
        view.isHidden = false
        lottie.play()
    }
    func stopAnimatingView() {
        lottie.stop()
        view.isHidden = true
    }
    
    init() {
        view.backgroundColor = .clear
        lottie.contentMode = .scaleAspectFit
        view.layout(lottie).topBottom().leftRight()
    }
}
