//
//  HomeFeatureTableViewCell.swift
//  Uthabo
//
//  Created by UTHABO on 2/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher
import Material

class FeaturedPhotoCollectionConstant {
    static let titleLabelHeaderHeight: CGFloat = 58
    static let totalItem: CGFloat = 10
    static let column: CGFloat = 2
    
    static let minLineSpacing: CGFloat = 10
    static let minItemSpacing: CGFloat = 10
    
    static let offset: CGFloat = 5 // TODO: for each side, define its offset
    
    static func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        
        // totalCellWidth = (collectionview width or tableview width) - (left offset + right offset) - (total space x space width)
        let totalWidth = boundWidth - (offset + offset) - ((column - 1) * minItemSpacing)
        
        return totalWidth / column
    }
    
    static let itemHeight: CGFloat = 200
}

class HomeFeatureTableViewCell: UITableViewCell {

    @IBOutlet weak var featuredTitle: UILabel!
    @IBOutlet weak var featuredCollectionView: UICollectionView!
    @IBOutlet weak var seeMoreButton: FlatButton!
    
    var contentArray = [ImageContentData]()
    var parent: UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        seeMoreButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 14.0)
        seeMoreButton.setTitleColor(UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.PinkColor), for: .normal)
        
        featuredCollectionView.isScrollEnabled = false
        featuredCollectionView.dataSource = self
        featuredCollectionView.delegate = self
        
        featuredCollectionView.register(UINib(nibName: "HomeFeatureCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeFeatureCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension HomeFeatureTableViewCell : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return contentArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : HomeFeatureCollectionViewCell? = featuredCollectionView.dequeueReusableCell(withReuseIdentifier: "HomeFeatureCollectionViewCell", for: indexPath) as? HomeFeatureCollectionViewCell
        
        let itemDic: ImageContentData = contentArray[indexPath.item]
        
        cell?.cellTitle.text = itemDic.title
        cell?.cellImageView.kf.indicatorType = .custom(indicator: ImageIndicator())
        cell?.cellImageView.kf.setImage(with: URL(string: itemDic.imageURL ?? ""))
        cell?.favouriteCount.text = String(itemDic.totalLike ?? 0)
        
        
        return cell!
    }
}

extension HomeFeatureTableViewCell : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        if let contentVC = sb.instantiateViewController(withIdentifier: "ImageDetailViewController") as? ImageDetailViewController {
            contentVC.imageData = contentArray[indexPath.item]
            
            parent?.navigationController?.pushViewController(contentVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: FeaturedPhotoCollectionConstant.offset*2, left: FeaturedPhotoCollectionConstant.offset, bottom: FeaturedPhotoCollectionConstant.offset*2, right: FeaturedPhotoCollectionConstant.offset)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return FeaturedPhotoCollectionConstant.minItemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return FeaturedPhotoCollectionConstant.minLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = FeaturedPhotoCollectionConstant.getItemWidth(boundWidth: collectionView.bounds.size.width)
        let cellHeight = FeaturedPhotoCollectionConstant.itemHeight
        return CGSize(width: cellWidth, height: cellHeight)
        
    }
}
