//
//  PasswordField.swift
//  Uthabo
//
//  Created by UTHABO on 25/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class PasswordField: CommonTextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        clearButtonMode = .whileEditing
        isVisibilityIconButtonEnabled = true
        visibilityIconButton?.tintColor = Color.green.base.withAlphaComponent(isSecureTextEntry ? 0.38 : 0.54)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        clearButtonMode = .whileEditing
        isVisibilityIconButtonEnabled = true
        visibilityIconButton?.tintColor = Color.green.base.withAlphaComponent(isSecureTextEntry ? 0.38 : 0.54)
    }

}
