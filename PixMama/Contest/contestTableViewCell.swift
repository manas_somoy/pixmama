//
//  contestTableViewCell.swift
//  Uthabo
//
//  Created by UTHABO on 9/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class contestTableViewCell: UITableViewCell {
    @IBOutlet weak var contestTitleLabel: UILabel!
    @IBOutlet weak var joinButton: FlatButton!
    @IBOutlet weak var timeLeftLabel: UILabel!
    @IBOutlet weak var prizeLabel: UILabel!
    @IBOutlet weak var infoButton: FlatButton!
    @IBOutlet weak var contestBackground: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        joinButton.layer.cornerRadius = joinButton.bounds.height/16
        joinButton.backgroundColor = Color.red.base
        joinButton.setTitle("JOIN", for: .normal)
        joinButton.setTitleColor(Color.white, for: .normal)
        
        infoButton.backgroundColor = Color(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.8)
        infoButton.setTitleColor(UIColor.hexStringToUIColor(hexString: "313131"), for: .normal)
        infoButton.layer.cornerRadius = infoButton.bounds.height/16
        infoButton.setTitle("INFO", for: .normal)
        
        contestTitleLabel.numberOfLines = 0
        contestTitleLabel.lineBreakMode = .byWordWrapping
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
