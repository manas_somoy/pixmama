//
//  AppDelegate.swift
//  PixMama
//
//  Created by UTHABO on 3/11/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import SwiftyJSON
import Material
import DropDown
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var user: UserData?
    private var bearerToken : String?

    //MARK:- TabBarController
    var AppTabController: UITabBarController?
    var tabArray = [UIViewController]()
    var tabBarNameArray = ["Home", "Search", "Contests", "Downloads"]

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        window = UIWindow.init(frame: UIScreen.main.bounds)
        Theme.isEnabled = false
        DropDown.startListeningToKeyboard()
        IQKeyboardManager.shared.enable = true
        
        if !isAppAlreadyLaunchedOnce() {
            changeRootToInitial()
        } else {
            SetupTabBarController()
            changeRootToTabVC()
        }
        
        return true
    }
    
    func isAppAlreadyLaunchedOnce() -> Bool{
        let uDefaults = UserDefaults.standard
        
        if let isAppAlreadyLaunchedOnce = uDefaults.string(forKey: FirstTime){
            print("App already launched : \(isAppAlreadyLaunchedOnce)")
            return true
        }else{
            uDefaults.set(true, forKey: FirstTime)
            print("App launched first time")
            return false
        }
    }
    
    func changeRootToInitial() {
        let signInNav = UINavigationController(rootViewController: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "InitialViewController"))
        signInNav.setNavigationBarHidden(true, animated: false)
        window?.rootViewController = signInNav
    }
    
    func changeRootToSignIn() {
        let signInNav = UINavigationController(rootViewController: UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController"))
        window?.rootViewController = signInNav
    }
    
    func changeRootToTabVC() {
        
        if user == nil {
            fetchUserData()
        }
        
        let drawerMenuVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DrawerViewController")
        let drawerControllerVC = AppNavigationDrawerViewController(rootViewController: AppTabController!, leftViewController: nil, rightViewController: drawerMenuVC)
        
        window?.rootViewController = drawerControllerVC
    }

    func SetupTabBarController() {
        AppTabController = UITabBarController()
        initiateMainViewControllers()
        AppTabController?.viewControllers = tabArray
        prepareTabBar()
    }
    
    fileprivate func prepareTabBar() {
        
        let appTabBar = AppTabController?.tabBar
        
        if let count = appTabBar?.items?.count {
            for i in 0...(count-1) {
                appTabBar?.items?[i].title = tabBarNameArray[i]
                
                appTabBar?.items?[i].image = UIImage(named: "iconTab\(i)")?.withRenderingMode(.alwaysOriginal)
                appTabBar?.items?[i].selectedImage = UIImage(named: "iconTab\(i)Selected")?.withRenderingMode(.alwaysOriginal)
            }
        }
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hexString: "000000")], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.hexStringToUIColor(hexString: "666666")], for: .normal)
    }
    
    fileprivate func initiateMainViewControllers() {
        
        if tabArray.count > 0 {
            tabArray.removeAll()
        }
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        
        if let homeVC = sb.instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController {
            tabArray.append(HomeNavigationController(rootViewController: homeVC))
        }
        
        if let searchVC = sb.instantiateViewController(withIdentifier: "SearchViewController") as? SearchViewController {
            tabArray.append(SearchNavigationController(rootViewController: searchVC))
        }
        
        if let contestVC = sb.instantiateViewController(withIdentifier: "ContestViewController") as? ContestViewController {
            tabArray.append(ContestNavigationViewController(rootViewController: contestVC))
        }
        
    }
    
    func saveBearerToken(Token token: String?) {
        if token != nil {
            UserDefaults.standard.set(token, forKey: UserConstants.UserDefaultKey.UserLoginToken)
        } else {
            print("Token not saved. Nil found.")
        }
    }
    
    func fetchBearerToken() -> String? {
        bearerToken = UserDefaults.standard.string(forKey: UserConstants.UserDefaultKey.UserLoginToken)
        return bearerToken
    }
    
    func saveUserData(UserData dataDictionary: JSON) {
        UserDefaults.standard.set(dataDictionary.dictionaryObject, forKey: UserConstants.UserDefaultKey.UserDictionary)
        
        user = UserData(jsonData: dataDictionary[UserConstants.UserDefaultKey.UserDictionary])
    }
    
    func fetchUserData() {
        
        if let dic = UserDefaults.standard.dictionary(forKey: UserConstants.UserDefaultKey.UserDictionary) {
            let dataDictionary = JSON(dic)
            user = UserData(jsonData: dataDictionary[UserConstants.UserDefaultKey.UserDictionary])
        } else {
            print("\nUser data not found in defaults!!!!!\n")
        }
    }
    
    func signOut() {
        user = nil
        UserDefaults.standard.removeObject(forKey: UserConstants.UserDefaultKey.UserDictionary)
        UserDefaults.standard.removeObject(forKey: UserConstants.UserDefaultKey.UserLoginToken)
    }

}

