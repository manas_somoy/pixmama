//
//  CommonTextField.swift
//  Uthabo
//
//  Created by UTHABO on 25/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class CommonTextField: ErrorTextField {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    fileprivate func commonInit() {
        isClearIconButtonEnabled = true
        placeholderNormalColor = Color.grey.darken1
        placeholderActiveColor = Color.grey.darken3
        dividerNormalColor = Color.grey.darken1
        dividerActiveColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.PinkColor)
        placeholderVerticalOffset = 10
        errorVerticalOffset = 0
        font = font?.withSize(15)
        addDoneButton()
    }
    
    func addDoneButton() {
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(donedatePicker(_:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        toolbar.setItems([spaceButton,doneButton], animated: false)
        
        // add toolbar to textField
        inputAccessoryView = toolbar
    }
    
    @objc
    func donedatePicker(_ sender: UIBarButtonItem) {
        if isFirstResponder {
            resignFirstResponder()
        }
    }
}
