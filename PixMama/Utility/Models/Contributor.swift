//
//  Contributor.swift
//  PixMama
//
//  Created by UTHABO on 21/11/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import Foundation
import SwiftyJSON

//MARK: Contriutor Model
class Contributor {
    
    fileprivate var _id: Int?
    fileprivate var _name: String?
    fileprivate var _email: String?
    fileprivate var _profileImageUrl: String?
    fileprivate var _contentCount: Int?
    fileprivate var _followersCount: Int?
    fileprivate var _followingCount: Int?
    
    var id: Int {
        get {
            return _id ?? Int.min
        }
    }
    
    var name: String {
        get {
            return _name ?? "User_\(id)"
        }
    }
    
    var email: String {
        get {
            return _email ?? "No Email"
        }
    }
    
    var profileImageUrl: String {
        get {
            return _profileImageUrl ?? ""
        }
    }
    
    var contentCount: String {
        get {
            return "\(_contentCount ?? 0)"
        }
    }
    
    var followersCount: String {
        get {
            return "\(_followersCount ?? 0)"
        }
    }
    
    var followingCount: String {
        get {
            return "\(_followingCount ?? 0)"
        }
    }
    
    init(jsonData: JSON) {
        _id = jsonData["id"].int
        _name = jsonData["name"].string
        _email = jsonData["email"].string
        _profileImageUrl = jsonData["image_url"].string
        _contentCount = jsonData["contents"].int
        _followersCount = jsonData["followers"].int
        _followingCount = jsonData["following"].int
    }
}

//MARK: ContributorArrayModel
class ContributorArray {
    
    var resultArray = [Contributor]()
    
    init(dataArray: [[String : Any]]?){
        if let data = dataArray {
            for item in data {
                let json = JSON(item)
                resultArray.append(Contributor(jsonData: json))
            }
        }
    }
}
