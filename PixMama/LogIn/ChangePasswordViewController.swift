//
//  ForgotPasswordViewController.swift
//  PixMama
//
//  Created by UTHABO on 22/12/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class ChangePasswordViewController: BaseViewController {

    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var oldTextField: PasswordField!
    @IBOutlet weak var newTextField: PasswordField!
    @IBOutlet weak var confirmationTextField: PasswordField!
    @IBOutlet weak var saveButton: FlatButton!
    
    var fromInitialView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        request?.delegate = self
        
        prepareNavigationItem()
        prepareContainerView()
        prepareTextViews()
        prepareButtons()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    @IBAction func saveButtonPressed(_ sender: FlatButton) {
        
        view.endEditing(true)
        
        if oldTextField.isEmpty {
            oldTextField.isErrorRevealed = true
            return
        } else {
            oldTextField.isErrorRevealed = false
        }
        
        if newTextField.isEmpty {
            newTextField.isErrorRevealed = true
            return
        } else {
            newTextField.isErrorRevealed = false
        }
        
        if confirmationTextField.isEmpty {
            confirmationTextField.isErrorRevealed = true
            return
        } else {
            confirmationTextField.isErrorRevealed = false
        }
        
        if confirmationTextField.text != newTextField.text {
            confirmationTextField.error = "Password Mismatch!"
            confirmationTextField.isErrorRevealed = true
            return
        } else {
            confirmationTextField.isErrorRevealed = false
            confirmationTextField.error = "This field must not be empty!!"
        }
        
        changePassword()
    }
    
    //MARK:- API Call
    fileprivate func changePassword() {
        
        request?.changePassword(Old: oldTextField.text!, New: newTextField.text!, Confirmation: confirmationTextField.text!, completion: { (dataDictionary) in
            
            let alert = UIAlertController.init(title: "Password Changed!", message: "Next time you sign in use your new password.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: { (ok) in
                if let signIn = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 2] {
                    self.navigationController?.popToViewController(signIn, animated: true)
                }
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            self.hideHud()
            
        }, failure: { (dataDictionary) in
            
            var message = ""
            if dataDictionary["errors"].dictionaryObject != nil {
                message += dataDictionary["errors"]["email"].arrayValue[0].stringValue
                if message.count > 0 {
                    message += dataDictionary["errors"]["token"].arrayValue[0].stringValue
                }
                message = "Verification operation failed."
            }
            
            let alert = UIAlertController.init(title: dataDictionary["message"].stringValue, message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            self.hideHud()
        })
    }
    
}

extension ChangePasswordViewController {
    
    fileprivate func prepareNavigationItem() {
        if !fromInitialView {
            navigationItem.titleLabel.text = "PIXMAMA"
            navigationItem.detailLabel.text = "Change Password"
            menuButton.isHidden = true
        } else {
            navigationItem.title = "Change Password"
        }
    }
    
    fileprivate func prepareContainerView() {
        containerView.cornerRadiusPreset = .cornerRadius4
        containerView.depthPreset = .depth2
        containerView.depth.radius = 10
    }
    
    fileprivate func prepareTextViews() {
        oldTextField.placeholder = "Enter Old Password"
        oldTextField.error = "This field must not be empty!!"
        newTextField.placeholder = "Enter New Password"
        newTextField.error = "This field must not be empty!!"
        confirmationTextField.placeholder = "Rewrite New Pasword"
        confirmationTextField.error = "This field must not be empty!!"
    }
    
    fileprivate func prepareButtons() {
        saveButton.setTitleColor(UIColor.white, for: .normal)
        saveButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        saveButton.backgroundColor = Color.red.base
        saveButton.cornerRadiusPreset = .cornerRadius2
    }
}

