//
//  HomeNavigationController.swift
//  Uthabo
//
//  Created by UTHABO on 2/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class HomeNavigationController: CommonNavigationController {
    
    override func prepare() {
        super.prepare()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return self.children.last?.preferredStatusBarStyle ?? .default }
}

