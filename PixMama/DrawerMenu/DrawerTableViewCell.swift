//
//  DrawerTableViewCell.swift
//  Uthabo
//
//  Created by UTHABO on 9/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class DrawerTableViewCell: UITableViewCell {

    @IBOutlet weak var menuItemImageView: UIImageView!
    @IBOutlet weak var menuItemTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        selectionStyle = .none
        menuItemImageView.tintColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.MotoTextColor)
        menuItemImageView.backgroundColor = UIColor.clear
        menuItemTitleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 16)
        menuItemTitleLabel.textColor = Color.grey.base
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            contentView.backgroundColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.PinkColor)
            menuItemTitleLabel.textColor = UIColor.white
            menuItemImageView.tintColor = UIColor.white
        } else {
            contentView.backgroundColor = .white
            menuItemTitleLabel.textColor = Color.grey.darken2
            menuItemImageView.tintColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.MotoTextColor)
        }
    }
    
}
