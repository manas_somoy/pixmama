//
//  DashDataTableViewCell.swift
//  Uthabo
//
//  Created by UTHABO on 31/10/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class DashDataTableViewCell: UITableViewCell {

    @IBOutlet weak var totalContentsView: UIView!
    @IBOutlet weak var totalContentsLabel: UILabel!
    @IBOutlet weak var contentsTitleLabel: UILabel!
    @IBOutlet weak var totalContestsView: UIView!
    @IBOutlet weak var totalContestsLabel: UILabel!
    @IBOutlet weak var contestsTitleLabel: UILabel!
    @IBOutlet weak var totalContributorsView: UIView!
    @IBOutlet weak var totalContributorsLabel: UILabel!
    @IBOutlet weak var contributorsTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        totalContentsView.cornerRadiusPreset = .cornerRadius1
        totalContentsView.depthPreset = .depth3
        
        totalContestsView.cornerRadiusPreset = .cornerRadius1
        totalContestsView.depthPreset = .depth3
        
        totalContributorsView.cornerRadiusPreset = .cornerRadius1
        totalContributorsView.depthPreset = .depth3
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
