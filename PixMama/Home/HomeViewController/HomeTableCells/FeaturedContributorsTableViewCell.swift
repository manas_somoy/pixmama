//
//  FeaturedContributorsTableViewCell.swift
//  PixMama
//
//  Created by UTHABO on 11/11/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import SwiftyJSON
import  Kingfisher

class TopContributorsConstant {
    static let titleLabelHeaderHeight: CGFloat = 58
    static let minLineSpacing: CGFloat = 10
    static let minItemSpacing: CGFloat = 10
    
    static let offset: CGFloat = 10 // TODO: for each side, define its offset
    
    static let itemWidth: CGFloat = 150
    static let itemHeight: CGFloat = 220
}

class FeaturedContributorsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var seeMoreButton: UIButton!
    @IBOutlet weak var contributorsCollectionView:
    UICollectionView!
    
    //Collection Data
    var contributors = [Contributor]()
    var parent: UIViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        contentView.backgroundColor = UIColor.hexStringToUIColor(hexString: "dadada")
        titleLabel.text = "Top Contributors"
        seeMoreButton.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 14.0)
        seeMoreButton.setTitleColor(UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.PinkColor), for: .normal)
        
        contributorsCollectionView.dataSource = self
        contributorsCollectionView.delegate = self
        contributorsCollectionView.register(UINib(nibName: "ContributorCollectionViewCell", bundle: nil
        ), forCellWithReuseIdentifier: "ContributorCollectionViewCell")
    }
    
}

extension FeaturedContributorsTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return contributors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = contributorsCollectionView.dequeueReusableCell(withReuseIdentifier: "ContributorCollectionViewCell", for: indexPath) as? ContributorCollectionViewCell
        
        let singleDetail = contributors[indexPath.row]
        
        cell?.contributorImageView.kf.setImage(with: URL(string: singleDetail.profileImageUrl), placeholder: UIImage(named: "iconHomeContributor"), options: nil, progressBlock: nil, completionHandler: nil)
        cell?.contributorNameLabel.text = singleDetail.name
        cell?.followersLabel.text = "\(singleDetail.contentCount) Contents"
        
        return cell!
    }
}

extension FeaturedContributorsTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let contributorVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContributorProfileViewController") as? ContributorProfileViewController {
            contributorVC.contributor = contributors[indexPath.item]
            parent?.navigationController?.pushViewController(contributorVC, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: TopContributorsConstant.offset, left: 15, bottom: TopContributorsConstant.offset, right: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return TopContributorsConstant.minItemSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return TopContributorsConstant.minLineSpacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: TopContributorsConstant.itemWidth, height: TopContributorsConstant.itemHeight)
    }
}
