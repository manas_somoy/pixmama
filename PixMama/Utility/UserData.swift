//
//  UserData.swift
//  Uthabo
//
//  Created by UTHABO on 26/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserData {
    
    private var _id: Int?
    private var _email: String?
    private var _user_name: String?
    private var _full_name: String?
    private var _imageUrl: String?
    
    var id: Int? {
        get {
            return _id
        }
        set(value) {
            _id = value
        }
    }
    
    var email: String? {
        get {
            return _email
        }
        set(value) {
            _email = value
        }
    }
    
    var user_name: String? {
        get {
            return _user_name
        }
        set(value) {
            _user_name = value
        }
    }
    
    var full_name: String? {
        get {
            return _full_name
        }
        set(value) {
            _full_name = value
        }
    }
    
    var imageUrl: String? {
        get {
            return _imageUrl
        }
        set(value) {
            _imageUrl = value
        }
    }
    
    init(jsonData: JSON) {
        
        id = jsonData["id"].intValue
        full_name = jsonData["full_name"].stringValue
        email = jsonData["email"].stringValue
        user_name = jsonData["user_name"].stringValue
        imageUrl = jsonData["imageUrl"].stringValue
    }
}
