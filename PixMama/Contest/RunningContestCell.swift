//
//  RunningContestCell.swift
//  Uthabo
//
//  Created by UTHABO on 10/10/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class RunningContestCell: UITableViewCell {
    
    fileprivate var card: ImageCard!
    
    /// Conent area.
    var cardImageView: UIImageView!
    var cardContentView: UILabel!
    
    /// Bottom Bar views.
    fileprivate var bottomBar: Bar!
    fileprivate var dateFormatter: DateFormatter!
    fileprivate var dateLabel: UILabel!
    fileprivate var favoriteButton: IconButton!
    fileprivate var shareButton: IconButton!
    
    /// Toolbar views.
    fileprivate var toolbar: Toolbar!
    fileprivate var moreButton: IconButton!
    
    lazy var imageUrl = String()
    lazy var dateString = String()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contentView.depthPreset = .depth1
        contentView.backgroundColor = Color.grey.lighten5
        
        prepareDateFormatter()
        prepareFavoriteButton()
        prepareShareButton()
        prepareMoreButton()
        prepareToolbar()
        prepareContentView()
        prepareBottomBar()
        preparePresenterCard()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        prepareImageView(imageUrl)
        prepareDateLabel(with: dateString)
    }
}

extension RunningContestCell {
    fileprivate func prepareImageView(_ with: String) {
        cardImageView = UIImageView()
        cardImageView.kf.setImage(with: URL(string: imageUrl), placeholder: nil, options: nil, progressBlock: nil) { (dimage, _, _, _) in
            self.cardImageView.image = dimage?.resize(toWidth: (self.superview?.layer.width)!)
        }
    }
    
    fileprivate func prepareDateFormatter() {
        dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
    }
    fileprivate func prepareDateLabel(with DateString: String) {
        dateLabel = UILabel()
        dateLabel.font = RobotoFont.regular(with: 12)
        dateLabel.textColor = Color.blueGrey.base
        dateLabel.textAlignment = .center
        dateLabel.text = dateFormatter.string(from: Date.distantFuture)
    }
    
    fileprivate func prepareFavoriteButton() {
        favoriteButton = IconButton(image: Icon.favorite, tintColor: Color.red.base)
    }
    
    fileprivate func prepareShareButton() {
        shareButton = IconButton(image: Icon.cm.share, tintColor: Color.blueGrey.base)
    }
    
    fileprivate func prepareMoreButton() {
        moreButton = IconButton(image: Icon.cm.moreHorizontal, tintColor: Color.blueGrey.base)
    }
    
    fileprivate func prepareToolbar() {
        toolbar = Toolbar(rightViews: [moreButton])
        toolbar.backgroundColor = nil
        
        toolbar.title = "Material"
        toolbar.titleLabel.textColor = .white
        toolbar.titleLabel.textAlignment = .center
        
        toolbar.detail = "Build Beautiful Software"
        toolbar.detailLabel.textColor = .white
        toolbar.detailLabel.textAlignment = .center
    }
    
    fileprivate func prepareContentView() {
        cardContentView = UILabel()
        cardContentView.numberOfLines = 0
        cardContentView.text = "Material is an animation and graphics framework that is used to create beautiful applications."
        cardContentView.font = RobotoFont.regular(with: 14)
    }
    
    fileprivate func prepareBottomBar() {
        bottomBar = Bar(leftViews: [favoriteButton], rightViews: [shareButton], centerViews: [dateLabel])
    }
    
    fileprivate func preparePresenterCard() {
        card = ImageCard()
        contentView.layout(card).leadingTrailing().topBottom()
        
        card.toolbar = toolbar
        card.toolbarEdgeInsetsPreset = .square3
        
        card.imageView = imageView
        
        card.contentView = cardContentView
        card.contentViewEdgeInsetsPreset = .square3
        
        card.bottomBar = bottomBar
        card.bottomBarEdgeInsetsPreset = .wideRectangle2
        
        contentView.layout(card).leadingTrailing(leading: 20, trailing: 20).center()
    }
}
