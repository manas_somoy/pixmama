//
//  DrawerViewController.swift
//  Uthabo
//
//  Created by UTHABO on 8/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import Kingfisher

@objc protocol DrawerViewControllerDelegate {
    
    @objc optional func pushOtherViews(index: IndexPath)
}

class DrawerViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var profileImageContainer: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profileTitleLabel: UILabel!
    @IBOutlet weak var profileDetailLabel: UILabel!
    @IBOutlet weak var menuTableview: UITableView!
    @IBOutlet weak var noUserView: UIView!
    
    fileprivate var menuItems = [String]()
    fileprivate var section1Items = [String]()
    fileprivate var section1Images = [String]()
    fileprivate var section2Items = [String]()
    fileprivate var section2Images = [String]()
    var drawerDelegate: DrawerViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setHeaderView()
        
        menuTableview.delegate = self
        menuTableview.dataSource = self
        menuTableview.separatorStyle = .none
        menuTableview.backgroundColor = .white
        menuTableview.bounces = false
        //need to add blur
        section1Items = ["Others Contests", "Create Contests", "Own Contests"]
        section1Images = ["iconDrawerOthersContest", "iconDrawerCreateContests", "iconDrawerOwnContests"]
        section2Items = [(delegate?.user != nil ? "Sign Out" : "Login"), "Privacy Policy", "About", "Help", "Check Update", "Rate Us"]
        section2Images = ["iconDrawerLogin", "iconDrawerPrivacy", "iconDrawerAbout", "iconDrawerHelp", "iconDrawerCheckUpdate", "iconDrawerRateUs"]
        
        setUserData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Gets called only once
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        section1Items = ["Others Contests", "Create Contests", "Own Contests"]
        section2Items = [(delegate?.user != nil ? "Sign Out" : "Login"), "Privacy Policy", "About", "Help", "Check Update", "Rate Us"]
        menuTableview.reloadRows(at: [IndexPath(row: menuTableview.numberOfRows(inSection: 0) - 1, section: 0)], with: .none)
        changeHeader()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUserData() {
        if let user = delegate?.user {
            profileTitleLabel.text = user.user_name ?? "user"
            profileDetailLabel.text = user.full_name ?? "Mr. User"
            
            profileImageView.kf.setImage(with: URL(string: user.imageUrl ?? ""), placeholder: UIImage(named: "imagePlaceHolderUser"), options: [], progressBlock: nil, completionHandler: nil)
        }
    }

    func changeHeader() {
        if delegate?.user?.id != nil {
            if noUserView.alpha == 1 {
                setUserData()
                noUserView.alpha = 0
            }
        } else {
            noUserView.alpha = 1
        }
    }

}

extension DrawerViewController {
    fileprivate func setHeaderView() {
        profileImageContainer.clipsToBounds = true
        profileImageContainer.layer.cornerRadius = profileImageContainer.bounds.height / 2
        profileImageContainer.layer.borderColor = Color.red.darken1.cgColor
        profileImageContainer.layer.borderWidthPreset = .border3
        profileImageView.layer.masksToBounds = true
        profileImageView.layer.cornerRadius = profileImageView.bounds.height / 2
        headerView.dividerThickness = 0.5
        headerView.dividerColor = Color.grey.lighten1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? section1Items.count : section2Items.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let sview = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 50))
            let slabel = UILabel()
            slabel.text = "Account"
            slabel.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
            slabel.textColor = Color.grey.base
            sview.layout(slabel).topBottom().leftRight(left: 20, right: 0)
            sview.dividerAlignment = .top
            sview.dividerThickness = 0.5
            sview.dividerColor = Color.grey.lighten1
            return sview
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 50
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: DrawerTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "DrawerTableViewCell") as? DrawerTableViewCell
        
        if cell == nil {
            let nibName = UINib.init(nibName: "DrawerTableViewCell", bundle: nil)
            
            // register nib
            tableView.register(nibName, forCellReuseIdentifier: "DrawerTableViewCell")
            cell  = tableView.dequeueReusableCell(withIdentifier: "DrawerTableViewCell") as? DrawerTableViewCell
            
        }
        
        if indexPath.section == 0 {
            cell?.menuItemTitleLabel.text = section1Items[indexPath.row]
            cell?.menuItemImageView.image = UIImage(named: section1Images[indexPath.row])
        } else {
            cell?.menuItemTitleLabel.text = section2Items[indexPath.row]
            cell?.menuItemImageView.image = UIImage(named: section2Images[indexPath.row])
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        drawerDelegate.pushOtherViews!(index: indexPath)
    }
}
