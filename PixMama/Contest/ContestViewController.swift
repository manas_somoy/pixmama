//
//  ContestViewController.swift
//  Uthabo
//
//  Created by UTHABO on 9/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import Kingfisher
import Alamofire
import SwiftyJSON

enum ContestType: String {
    case Running = "RUNNING"
    case Upcoming = "UPCOMING"
    case Finised = "FINISHED"
}

class ContestViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var contestTableView: TableView!
    fileprivate var buttons = [TabItem]()
    fileprivate var typeTabBar: TabBar!
    
    fileprivate var contestArray = [[String:Any]]()
    
    fileprivate var runningContestArray = [[String:Any]]()
    fileprivate var upcomingContestArray = [[String:Any]]()
    fileprivate var finishedContestArray = [[String:Any]]()
    
    fileprivate var runPagination = Pagination()
    fileprivate var upPagination = Pagination()
    fileprivate var finPagination = Pagination()
    
    fileprivate var TypeOfContest: ContestType = .Running
    fileprivate var currentPagination: Pagination?
    
    fileprivate var primaryCall = true
    
    fileprivate lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = UIColor.red
        
        return refreshControl
    }()
    fileprivate lazy var noDataLabel = UILabel()
    
    fileprivate let itemCount = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        request?.delegate = self
        
        prepareNavigationItem()
        prepareTabButtons()
        prepareTabBar()
        prepareNodataLabel()
        
        contestTableView.contentInset = EdgeInsets(top: CGFloat(typeTabBar.heightPreset.rawValue) + 20, left: 0, bottom: 30, right: 0)
        contestTableView.delegate = self
        contestTableView.dataSource = self
        contestTableView.separatorStyle = .none
        contestTableView.allowsSelection = false
        contestTableView.refreshControl = refreshControl
        
        currentPagination = runPagination
        fetchContest()
    }
    
    @objc
    fileprivate func handleRefresh(_ refreshControl: UIRefreshControl) {
        currentPagination?.resetPagination()
        setContestType()
    }
    
    fileprivate func clearTable() {
        contestArray.removeAll()
        contestTableView.reloadData()
    }
    
    //MARK:- Api Call
    fileprivate func setContestType() {
        
        if typeTabBar.selectedTabItem == buttons[0] {
            TypeOfContest = .Running
            currentPagination = runPagination
            if refreshControl.isRefreshing {
                runningContestArray.removeAll()
            }
            
            if runningContestArray.count == 0 {
                fetchContest()
            } else {
                contestArray.append(contentsOf: runningContestArray)
            }
        } else if typeTabBar.selectedTabItem == buttons[1] {
            TypeOfContest = .Upcoming
            currentPagination = upPagination
            if refreshControl.isRefreshing {
                upcomingContestArray.removeAll()
            }
            
            if upcomingContestArray.count == 0 {
                fetchContest()
            } else {
                contestArray.append(contentsOf: upcomingContestArray)
            }
        } else if typeTabBar.selectedTabItem == buttons[2] {
            TypeOfContest = .Finised
            currentPagination = finPagination
            if refreshControl.isRefreshing {
                finishedContestArray.removeAll()
            }
            
            if finishedContestArray.count == 0 {
                fetchContest()
            } else {
                contestArray.append(contentsOf: finishedContestArray)
            }
        }
        
        if primaryCall == false {
            noDataLabel.isHidden = true
            contestTableView.reloadData()
        }
    }
    
    fileprivate func fetchContest() {
        noDataLabel.isHidden = true
        primaryCall = true
        DispatchQueue.main.async {
            self.request?.getContests(ByType: self.TypeOfContest.rawValue, PageNo: (self.currentPagination?.currentPageNo)!, ItemsCount: self.itemCount)
        }
    }
    
    //MARK:- ApiRequestingDelegate
    func fetchContestApiCompleted(DataDictionary dataDictionary: [String : Any]?) {
        primaryCall = false
        hideRefresh()
        if let dic = dataDictionary {
            let data = JSON(dic)
            changePaginationData(paginationJSON: data["pagination"])
            
            if let dataArray = data["data"]["contests"].arrayObject as? [[String: Any]] {
                if dataArray.count > 0 {
                    if TypeOfContest == .Running {
                        runningContestArray.append(contentsOf: dataArray)
                    } else if TypeOfContest == .Upcoming {
                        upcomingContestArray.append(contentsOf: dataArray)
                    } else if TypeOfContest == .Finised {
                        finishedContestArray.append(contentsOf: dataArray)
                    }
                    contestArray.append(contentsOf: dataArray)
                    contestTableView.reloadData()
                } else {
                    noDataLabel.isHidden = false
                }
            }
        }
    }
    
    func fetchContestApiFailed(DataDictionary dataDictionary: [String : Any]?) {
        primaryCall = false
        hideRefresh()
        
    }
    
    func hideRefresh() {
        if refreshControl.isRefreshing {
            refreshControl.endRefreshing()
        }
    }
    
    func changePaginationData(paginationJSON: JSON) {
        
        currentPagination?.has_pivot = paginationJSON["has_more_pages"].boolValue
        currentPagination?.total = paginationJSON["total_items_count"].intValue
        currentPagination?.currentPageNo = paginationJSON["current_page_no"].intValue
        currentPagination?.lastPageNo = paginationJSON["last_page_no"].intValue
        let itemCount = paginationJSON["items_per_page"].intValue
        
        if (currentPagination?.total)! > (currentPagination?.count)! + itemCount {
            currentPagination?.count += itemCount
        } else {
            currentPagination?.count = (currentPagination?.total)!
        }
    }
}

extension ContestViewController {
    fileprivate func prepareNavigationItem() {
        
        navigationItem.titleLabel.text = "CONTEST"
        navigationItem.detailLabel.text = "Join to win prizes"
    }
    
    fileprivate func prepareTabButtons() {
        let btn1 = TabItem(title: "Running")
        btn1.titleLabel?.fontSize = 15
        btn1.setTitleColor(Color.red.base, for: .selected)
        btn1.pulseAnimation = .backing
        buttons.append(btn1)
        
        let btn2 = TabItem(title: "Upcoming")
        btn2.titleLabel?.fontSize = 15
        btn2.setTitleColor(Color.red.base, for: .selected)
        btn2.pulseAnimation = .backing
        buttons.append(btn2)
        
        let btn3 = TabItem(title: "Finished")
        btn3.titleLabel?.fontSize = 15
        btn3.setTitleColor(Color.red.base, for: .selected)
        btn3.pulseAnimation = .backing
        buttons.append(btn3)
    }
    
    fileprivate func prepareTabBar() {
        typeTabBar = TabBar()
        typeTabBar.delegate = self
        
        typeTabBar.depthPreset = .depth2
        
        typeTabBar.dividerColor = Color.grey.lighten1
        typeTabBar.dividerAlignment = .bottom
        
        typeTabBar.lineColor = Color.red.base
        typeTabBar.lineAlignment = .bottom
        
        typeTabBar.backgroundColor = Color.grey.lighten5
        typeTabBar.tabItems = buttons
        
        view.layout(typeTabBar).leadingTrailing().top()
    }
    
    fileprivate func prepareNodataLabel() {
        
        noDataLabel.font = RobotoFont.bold(with: 21.0)
        noDataLabel.textColor = Color.blueGrey.base
        noDataLabel.isHidden = true
        noDataLabel.textAlignment = .center
        view.addSubview(noDataLabel)
        view.bringSubviewToFront(noDataLabel)
        view.layout(noDataLabel).leadingTrailing().top(100)
        noDataLabel.text = "No Contest found for this group."
    }
}

extension ContestViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contestArray.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 207
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: contestTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "contestTableViewCell") as? contestTableViewCell
        
        if cell == nil {
            let nibName = UINib(nibName: "contestTableViewCell", bundle: nil)
            
            tableView.register(nibName, forCellReuseIdentifier: "contestTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "contestTableViewCell") as? contestTableViewCell
        }
        
        
        let singleItem = JSON(contestArray[indexPath.row])
        
        cell?.contestTitleLabel.text = singleItem["title"].stringValue
        print(singleItem["contest_end"].stringValue)
        cell?.timeLeftLabel.text = Utility.contestTimeLeft(EndDate: singleItem["contest_end"].stringValue)
        cell?.contestBackground.kf.setImage(with: URL(string: singleItem["banner"].stringValue))
        
        
        return cell!
    }
}

extension ContestViewController: TabBarDelegate {
    
    func tabBar(tabBar: TabBar, shouldSelect tabItem: TabItem) -> Bool {
        if tabBar.selectedTabItem == tabItem {
            return false
        } else {
            return true
        }
    }
    
    func tabBar(tabBar: TabBar, willSelect tabItem: TabItem) {
        clearTable()
        if primaryCall {
            Alamofire.SessionManager.default.session.getAllTasks { tasks in
                tasks.forEach { $0.cancel() }
            }
        }
    }
    
    func tabBar(tabBar: TabBar, didSelect tabItem: TabItem) {
        setContestType()
    }
}
