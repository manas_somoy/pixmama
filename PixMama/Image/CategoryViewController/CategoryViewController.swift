//
//  CategoryViewController.swift
//  Uthabo
//
//  Created by UTHABO on 15/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import Kingfisher
import SwiftyJSON

class CategoryViewController: BaseViewController{
    
    var slug : String?
    var categoryName : String?
    
    var otherCategoryList = [GroupCategory]()
    var promotedCategoryList = [GroupCategory]()
    
    @IBOutlet weak var categoryTableView: UITableView!
    fileprivate lazy var noDataLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareNavigationButtons()
        prepareNavigationItem()
        prepareNodataLabel()
        
        request?.delegate = self
        
        categoryTableView.delegate = self
        categoryTableView.dataSource = self
        categoryTableView.separatorStyle = .none
        
        fetchCategories()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension CategoryViewController {
    
    fileprivate func prepareNavigationItem() {
        
        navigationItem.titleLabel.text = "\(categoryName ?? "Categories")"
        navigationItem.detailLabel.text = "Choose category"
    }
    
    fileprivate func prepareNavigationButtons() {
//        navigationItem.backButton.tintColor = Color.grey.darken4
    }
    
    fileprivate func prepareNodataLabel() {
        
        noDataLabel.font = RobotoFont.bold(with: 21.0)
        noDataLabel.textColor = Color.blueGrey.base
        noDataLabel.isHidden = true
        noDataLabel.textAlignment = .center
        view.addSubview(noDataLabel)
        view.bringSubviewToFront(noDataLabel)
        view.layout(noDataLabel).leadingTrailing().top(200)
        noDataLabel.text = "Nothing matched with your search."
    }
}

//MARK:- Api Call
extension CategoryViewController {
    fileprivate func fetchCategories() {
        showHUD()
        DispatchQueue.main.async {
            if let slug = self.slug {
                self.request?.fetchCategoriesForGroup(groupSlug: slug)
            }
        }
    }
}

//MARK:- APIRequestingDelegate
extension CategoryViewController {
    
    func fetchCategoriesApiCompleted(DataDictionary dataDictionary: [String : Any]?) {
        if let dic = dataDictionary {
            let data = JSON(dic)
            for item in data["data"]["promoted_categories"].arrayValue {
                let category = GroupCategory(jsonData: item)
                promotedCategoryList.append(category)
            }
            
            categoryTableView.reloadData()
        }
        
        if promotedCategoryList.count == 0 {
            noDataLabel.isHidden = false
        }
        hideHud()
    }
    
    func fetchCategoriesApiFailed(DataDictionary dataDictionary: [String : Any]?) {
        print("fetchCategoriesApiFailed ========>>>>>>>> \(String(describing: dataDictionary))")
        noDataLabel.isHidden = false
        hideHud()
    }
}

//MARK: TableView Delegate & DataSource
extension CategoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return promotedCategoryList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCategoryCell") as? GroupCategoryCell
        
        let singleCategory = promotedCategoryList[indexPath.row]
        cell?.catTitleLabel.text = singleCategory.name
        if let url = singleCategory.image_url {
            cell?.catImageView.kf.setImage(with: URL(string: url))
        }
        
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let contentVC = storyboard?.instantiateViewController(withIdentifier: "ImageContentViewController") as? ImageContentViewController {
            contentVC.groupSlug = slug
            
            let singleCategory = promotedCategoryList[indexPath.row]
            contentVC.categorySlug = singleCategory.slug
            contentVC.categoryName = singleCategory.name
            navigationController?.pushViewController(contentVC, animated: true)
        }
    }
}


class GroupCategory {
    
    var slug: String?
    var image_url: String?
    var id: Int?
    var name: String?
    
    init(jsonData: JSON) {
        slug = jsonData["slug"].stringValue
        id = jsonData["id"].intValue
        image_url = jsonData["image_url"].stringValue
        name = jsonData["name"].stringValue
    }
}
