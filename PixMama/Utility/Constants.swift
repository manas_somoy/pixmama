//
//  Constants.swift
//  Motomez
//
//  Created by MazeGeek iOS  on 7/20/18.
//  Copyright © 2018 Amit Biswas. All rights reserved.
//

import Foundation

let FirstTime = "Firsttime"

let CONTENT_BASEURL = "https://api.pixmama.com/content/"
let LOGIN_BASEURL = "https://api.pixmama.com/buyer/"
let CONTEST_BASEURL = "https://api.pixmama.com/contest/"
let DASHBOARD_BASEURL = "https://api.pixmama.com/data/"
let CONTRIBUTOR_BASEURL = "https://api.pixmama.com/contributor/"
let SEARCH_BASEURL = "https://api.pixmama.com/api/search?"
let BUYER_BASEURL = "https://api.pixmama.com/buyer/"

struct APIUrls {
    
    static let BUYER_REGISTRATION = "register"
    static let VERIFY_EMAIL = "email/verify"
    static let FORGOT_PASSWORD = "change/password"
    static let BUYER_LOGIN = "oauth/token"
    static let COUNTRY = "get-countries"
    static let STATE = "get-states"
    
    static let SEARCH_URL = "https://api.pixmama.com/api/search?"
    
    static let GET_GROUPS = "groups"
    static let GET_LATEST_IMAGES = "latest-images"
    static let TOTAL_DATA = "total-count"
    static let GET_CATEGORIES = "group/categories"
    static let GET_CONTENTS = "group/category/list"
    static let GET_CONTENT_DETAIL = "details"
    
    static let GET_CONTRIBUTOR_LIST = "list"
    static let GET_CONTRIBUTOR_DETAILS = "details"
    static let GET_FEATURED_CONTRIBUTORS = "featured"
    static let GET_CONTRIBUTOR_CONTENTS = "contents"
    
    static let GET_CONTEST_CATEGORIES = "categories"
    static let GET_CONTEST = "getContests"
    static let GET_CONTEST_DETAILS = "getContestDetails"
    static let CREATE_CONTEST = "https://api.pixmama.com/contest"
    static let GET_USER_CONTEST = "getCreatorContests"
    
    static let FOLLOW_CONTRIBUTOR = "follow-contributor"
    static let UNFOLLOW_CONTRIBUTOR = "unfollow-contributor"
    static let ORDER_HISTORY = "order-history"
    static let BUYER_CONTENT = "contents"
    
    static let CREATE_ORDER_URL = "https://api.pixmama.com/order/type/standalone/create"
}

struct WebPageURLS {
    static let PRIVACY_POLICY = "https://pixmama.com/privacy-policy"
    static let ABOUT = "https://pixmama.com/about"
    static let HELP = "https://pixmama.com/faq"
}

struct ColorConstants {
    
    struct AppColor {
        static let MamaRed = "bf2c2f"
        static let BaseViewColor = "F4F7F6"
        static let CreamyBaseColor = "fffcf6"
        static let ForwardButtonColor = "244769"
        static let MotoTextColor = "262626"
        static let NavigtaionColor = "34B67F"
        static let RedColor = "E24F71"
        static let PinkColor = "e53b67"
        static let Green = "57e86b"
        static let FollowBlue = "1DA1F2"
    }
}

struct UserConstants {
    struct UserDefaultKey {
        static let UserLoginToken = "Bearer"
        static let UserDictionary = "user"
        static let UserName = "user_name"
        static let FullName = "full_name"
        static let ID = "id"
        static let Email = "email"
        static let Groups = "groups"
    }
}

struct Milliseconds {
    static let ONE_WEEK = 604800000
    static let ONE_DAY = 86400000
    static let ONE_HOUR = 3600000
    static let ONE_MINUTE = 60000
}

struct  AlertConstants {
    struct InternetConnectivity {
        static let TITLE = "You are not connected!"
        static let MESSAGE = "Check your internet and try again."
    }
    
    struct ContentNotFound {
        static let TITLE = "Content not found."
        static let MESSAGE = "No content found. Try again."
    }
    
    struct ServerError {
        static let TITLE = "Something went wrong!"
        static let MESSAGE = "Can not communicate with server. Please try checking your connection."
    }
    
    struct OperationFailed {
        static let TITLE = "Operation failed.!"
        static let MESSAGE = "Can not complete the operation. Please try again."
    }
    
    struct RequestTimeOut {
        static let TITLE = "Timeout Error."
        static let MESSAGE = "The request timed out."
    }
    
    struct UnauthorizedRequest {
        static let TITLE = "Unauthorized Request."
        static let MESSAGE = "You are not authorized to send request to server."
    }
    
    struct NoDataFound {
        static let TITLE = "No Data Found!"
        static let MESSAGE = "Sorry, we could find any data for this request. Try again later."
    }
}
