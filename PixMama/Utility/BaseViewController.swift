//
//  BaseViewController.swift
//  Uthabo
//
//  Created by UTHABO on 15/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import Lottie
import MBProgressHUD

class AppNavigationDrawerViewController: NavigationDrawerController {
    
    override func prepare() {
        super.prepare()
        
        Application.statusBarStyle = .default
        isHiddenStatusBarEnabled = false
    }
}

class CommonNavigationController: NavigationController {
    
    override func prepare() {
        super.prepare()
        
        guard let v = navigationBar as? NavigationBar else {
            return
        }
        
        v.backgroundColor = Color.white
        v.depthPreset = .depth1
        v.dividerColor = Color.grey.lighten3
        v.dividerThickness = 1
        
        isMotionEnabled = true
    }
}

class CommonUINavigationController: UINavigationController {
    
    var subTitle: String = "" {
        didSet {
            navigationItem.setTitleWith(title: navigationItem.title ?? "", subtitle: subTitle)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
}

class CommonScrollView: UIScrollView {
    
    override func touchesShouldBegin(_ touches: Set<UITouch>, with event: UIEvent?, in view: UIView) -> Bool {
        return true
    }
}

class BaseViewController: UIViewController, ApiRequestingDelegate, UINavigationControllerDelegate, NavigationDrawerControllerDelegate {
    
    var delegate: AppDelegate?
    var request: ApiRequest?
    
    var menuButton: IconButton!
    
    var mbHud: MBProgressHUD!
    let loadingView = LOTAnimationView(name: "mamaLoader")

    override func viewDidLoad() {
        super.viewDidLoad()

        prepareNavigationButtons()
        prepareNavigationItem()
        
        view.backgroundColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.BaseViewColor)
        
        delegate = UIApplication.shared.delegate as? AppDelegate
        
        request = ApiRequest()
        request?.delegate = self
        
        navigationController?.delegate = self
        navigationDrawerController?.delegate = self
        
        loadingView.loopAnimation = true
        loadingView.animationSpeed = 2
        loadingView.contentMode = .scaleAspectFill
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        viewController.edgesForExtendedLayout = []
        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        viewController.navigationItem.backBarButtonItem = item
    }
    
    func internetConnectivityError() {
        hideHud()
        DispatchQueue.main.async(execute: {
            print("no internet =============> sorry")
            let alert = UIAlertController(title: AlertConstants.InternetConnectivity.TITLE, message: AlertConstants.InternetConnectivity.MESSAGE, preferredStyle: .alert)
            let yesButton = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
                //Handle your yes please button action here
            })
            alert.addAction(yesButton)
            self.present(alert, animated: true)
        })
    }
    
    func operationFailed() {
        hideHud()
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: AlertConstants.OperationFailed.TITLE, message: AlertConstants.OperationFailed.MESSAGE, preferredStyle: .alert)
            let yesButton = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
                //Handle your yes please button action here
            })
            alert.addAction(yesButton)
            self.present(alert, animated: true)
        })
    }
    
    func contentNotFound() {
        hideHud()
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: AlertConstants.ContentNotFound.TITLE, message: AlertConstants.ContentNotFound.MESSAGE, preferredStyle: .alert)
            let yesButton = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
                //Handle your yes please button action here
            })
            alert.addAction(yesButton)
            self.present(alert, animated: true)
        })
    }
    
    func serverError() {
        hideHud()
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: AlertConstants.ServerError.TITLE, message: AlertConstants.ServerError.MESSAGE, preferredStyle: .alert)
            let yesButton = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
                //Handle your yes please button action here
            })
            alert.addAction(yesButton)
            self.present(alert, animated: true)
        })
    }
    
    func timeoutError() {
        hideHud()
        DispatchQueue.main.async(execute: {
            
            let alert = UIAlertController(title: AlertConstants.RequestTimeOut.TITLE, message: AlertConstants.RequestTimeOut.MESSAGE, preferredStyle: .alert)
            let yesButton = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
                //Handle your yes please button action here
            })
            alert.addAction(yesButton)
            self.present(alert, animated: true)
        })
    }
    
    func unauthorizedError() {
        hideHud()
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: AlertConstants.UnauthorizedRequest.TITLE, message: AlertConstants.UnauthorizedRequest.MESSAGE, preferredStyle: .alert)
            let yesButton = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: { action in
                //Handle your yes please button action here
            })
            alert.addAction(yesButton)
            self.present(alert, animated: true)
        })
    }
}

extension BaseViewController {
    
    fileprivate func prepareNavigationItem() {
        
        navigationItem.rightViews = [menuButton]
    }
    
    fileprivate func prepareNavigationButtons() {
        navigationItem.backButton.tintColor = Color.grey.darken4
        menuButton = IconButton(image: Icon.cm.menu?.tint(with: Color.grey.darken4))
        menuButton.addTarget(self, action: #selector(showMenuFromLeft(_:)), for: .touchUpInside)
        
    }
    
    @objc
    fileprivate func showMenuFromLeft(_ button: IconButton) {
        if let rightView = navigationDrawerController?.rightViewController as? DrawerViewController {
            rightView.drawerDelegate = self
            rightView.view.setNeedsLayout()
        }
        navigationDrawerController?.openRightView()
    }
}

extension BaseViewController: DrawerViewControllerDelegate {
    
    func pushOtherViews(index: IndexPath) {
        
        switch index.section {
        case 0:
            switch index.row {
            case 1:
                print(index)
                if let createContest = storyboard?.instantiateViewController(withIdentifier: "CreatContestViewController") as? CreatContestViewController {
                    navigationController?.pushViewController(createContest, animated: true)
                }
            default:
                print("\nDrawer Section \(index.section) no row!!!\n")
            }
        case 1:
            switch index.row {
            case 0:
                if delegate?.user == nil {
                    let signIn = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController") as? SignInViewController
                    navigationController?.pushViewController(signIn!, animated: true)
                } else {
                    delegate?.signOut()
                }
            case 1:
                print("Privacy Policy")
                UIApplication.shared.open(URL(string: WebPageURLS.PRIVACY_POLICY)!)
            case 2:
                print("About")
                UIApplication.shared.open(URL(string: WebPageURLS.ABOUT)!)
            case 3:
                print("Help")
                UIApplication.shared.open(URL(string: WebPageURLS.HELP)!)
            case 4:
                print("")
            case 5:
                print("")
                
            default:
                print("\nDrawer Section \(index.section) no row!!!\n")
            }
        default:
            print("\nNo drawer section!!!!\n")
        }
        
//        switch index {
//        case 0:
//            print(index)
//            if let createContest = storyboard?.instantiateViewController(withIdentifier: "CreatContestViewController") as? CreatContestViewController {
//                navigationController?.pushViewController(createContest, animated: true)
//            }
//        case 1:
//            if delegate?.user == nil {
//                let signIn = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController") as? SignInViewController
//                navigationController?.pushViewController(signIn!, animated: true)
//            } else {
//                delegate?.signOut()
//            }
//
//        default:
//            print("default")
//        }
        
        navigationDrawerController?.closeRightView()
    }
}

//MARK: Loading animation
extension BaseViewController {
    
    func showHUD() {
        UIApplication.shared.beginIgnoringInteractionEvents()
        mbHud = MBProgressHUD.showAdded(to: view, animated: true)
        mbHud.backgroundView.blurEffectStyle = .prominent
        mbHud.mode = .customView
        loadingView.layout.height(50).width(50)
        loadingView.play()
        mbHud.customView = loadingView
        mbHud.label.text = "Loading..."
    }
    
    func hideHud() {
        mbHud.hide(animated: true)
        loadingView.stop()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func showLoading() {
        if !loadingView.isDescendant(of: view) {
            view.addSubview(loadingView)
            view.layout(loadingView).center()
            view.bringSubviewToFront(loadingView)
            loadingView.play()
        }
    }
    
    func hideLoading() {
        if loadingView.isDescendant(of: view) {
            loadingView.removeFromSuperview()
            loadingView.stop()
        }
    }
}
