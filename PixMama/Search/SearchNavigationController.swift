//
//  SearchNavigationController.swift
//  Uthabo
//
//  Created by UTHABO on 7/10/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material

class SearchNavigationController: CommonNavigationController {

    override func prepare() {
        super.prepare()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return self.children.last?.preferredStatusBarStyle ?? .default }
    
}
