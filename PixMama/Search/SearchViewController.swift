//
//  SearchMediaViewController.swift
//  Uthabo
//
//  Created by UTHABO on 2/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import Alamofire
import SwiftyJSON
import Kingfisher

class SearchViewController: BaseViewController {

    fileprivate let searchBar = SearchBar()
    @IBOutlet weak var searchTableView: UITableView!
    fileprivate lazy var noDataLabel = UILabel()
    
    //Group Radio Buttons
    var radioArray = [RadioButton]()
    
    fileprivate var selectedButton: RadioButton? {
        return radioArray.first { $0.isSelected }
    }
    
    //Variables
    var groups = [Group]()
    var tableDataSource = [Any]()
    var searchedData = [SearchedData]()
    var initialData = [GroupCategory]()
    
    var typedString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        request?.delegate = self
        
        prepareNavigationItem()
        prepareSearchBar()
        prepareTextField()
        prepareSearchSection()
        prepareNodataLabel()
        
        searchGroupCategories()
        
        searchTableView.delegate = self
        searchTableView.dataSource = self
        searchTableView.separatorStyle = .none
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Radio Button Tapped
    @objc
    func radioButtonSelected(_ sender: RadioButton) {
        
        radioArray.forEach { $0.setSelected($0 == sender, animated: true) }
        searchGroupCategories()
    }
    
    //MARK: API Call
    fileprivate func searchGroupCategories() {
        noDataLabel.isHidden = true
        showHUD()
        if searchBar.textField.text == nil || searchBar.textField.text == "" {
            initialData.removeAll()
            searchTableView.reloadData()
            
            let grpData = groups[selectedButton?.tag ?? 0]
            DispatchQueue.main.async {
                self.request?.fetchCategoriesForGroup(groupSlug: grpData.slug!)
            }
        } else {
            searchQuery(query: typedString)
        }
    }
    
    @objc
    fileprivate func searchQuery(query: String) {
        noDataLabel.isHidden = true
        showHUD()
        searchedData.removeAll()
        searchTableView.reloadData()
        DispatchQueue.main.async {
            let groupSlug = self.groups[self.selectedButton?.tag ?? 0].slug ?? "image"
            self.request?.searchContentWith(Query: query, Group: groupSlug)
        }
    }
    
    //MARK: APIRequestingDelegate
    func fetchSearchApiCompleted(DataDictionary dataDictionary: [Any]?) {
        
        if let data = dataDictionary {
            for item in data {
                
                let json = JSON(item)
                searchedData.append(SearchedData(jsonData: json))
            }
            searchTableView.reloadData()
        }
        
        if searchedData.count == 0 {
            noDataLabel.isHidden = false
        }
        hideHud()
    }
    
    func fetchSearchApiFailed(DataDictionary dataDictionary: [Any]?) {
        print("\nfetchSearchApiFailed\n")
        
        noDataLabel.isHidden = false
        hideHud()
    }
    
    func fetchCategoriesApiCompleted(DataDictionary dataDictionary: [String : Any]?) {
        if let dic = dataDictionary {
            let data = JSON(dic)
            for item in data["data"]["promoted_categories"].arrayValue {
                let category = GroupCategory(jsonData: item)
                initialData.append(category)
            }
            
            searchTableView.reloadData()
        }
        
        if initialData.count == 0 {
            noDataLabel.isHidden = false
        }
        hideHud()
    }
    
    func fetchCategoriesApiFailed(DataDictionary dataDictionary: [String : Any]?) {
        print("fetchCategoriesApiFailed ========>>>>>>>> \(String(describing: dataDictionary))")
        noDataLabel.isHidden = false
        hideHud()
    }
}

extension SearchViewController: SearchBarDelegate {
    fileprivate func prepareSearchBar() {
        searchBar.depthPreset = .depth2
        searchBar.backgroundColor = nil
        searchBar.delegate = self
    }
    
    fileprivate func prepareTextField() {
        let leftView = UIImageView()
        leftView.image = Icon.cm.search?.tint(with: UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.PinkColor))
        searchBar.textField.leftView = leftView
        searchBar.textField.leftViewMode = .always
        
        searchBar.motionIdentifier = "searchBar"
        searchBar.textField.cornerRadiusPreset = .cornerRadius2
        searchBar.textField.backgroundColor = UIColor.white//hexStringToUIColor(hexString: ColorConstants.AppColor.BaseViewColor)
        searchBar.textField.delegate = self
        searchBar.textField.enablesReturnKeyAutomatically = true
    }
    
    fileprivate func prepareNavigationItem() {
//        navigationItem.centerViews = [searchBar]
        
        navigationItem.titleLabel.text = "Pixmama"
        navigationItem.detailLabel.text = "Search Contents"
    }
    
    fileprivate func prepareSearchSection() {
    
        view.addSubview(searchBar)
        view.layout(searchBar).top().leadingTrailing()
        
        //create radio button according to saved group object
        if let groupData = UserDefaults.standard.array(forKey: UserConstants.UserDefaultKey.Groups) {
        
            for index in 0...(groupData.count - 1) {
                
                let grp = Group(jsonData: JSON(groupData[index]))
                groups.append(grp)
                
                let radio = RadioButton()
                radio.titleLabel?.font = UIFont(name: "Avenir-Medium", size: 12)
                radio.titleColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.MotoTextColor)
                radio.setIconColor(UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.PinkColor), for: .selected)
                radio.tag = index
                if let name = grp.name {
                    radio.title = name
                }
                
                radio.setSelected(index == 0, animated: false)
                radio.addTarget(self, action: #selector(radioButtonSelected(_:)), for: .touchUpInside)
                radioArray.append(radio)
            }
            
            //stackview to add group radiobuttons
            let radioStack = UIStackView(arrangedSubviews: radioArray)
            radioStack.axis = .horizontal
            radioStack.distribution = .fillEqually
            radioStack.alignment = .leading
            radioStack.spacing = 0
            
            view.addSubview(radioStack)
            view.layout(radioStack).top(searchBar.bounds.height).leading(5).height(33)
            
            //stackview background
            let subview = UIView(frame: radioStack.bounds)
            subview.backgroundColor = UIColor.white//hexStringToUIColor(hexString: ColorConstants.AppColor.BaseViewColor)
            view.addSubview(subview)
            view.layout(subview).top(searchBar.bounds.height).leadingTrailing(leading: 5, trailing: 5).height(33)
            subview.cornerRadiusPreset = .cornerRadius2
            subview.depthPreset = .depth2
            view.bringSubviewToFront(radioStack) //bring stackview in front of background
            
            let topInset = searchBar.frame.height + 33
            searchTableView.contentInset = UIEdgeInsets(top: topInset, left: 0, bottom: 10, right: 0)
            searchTableView.scrollIndicatorInsets = UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)
        }
    }
    
    fileprivate func prepareNodataLabel() {
        
        noDataLabel.font = RobotoFont.bold(with: 21.0)
        noDataLabel.textColor = Color.blueGrey.base
        noDataLabel.isHidden = true
        noDataLabel.textAlignment = .center
        view.addSubview(noDataLabel)
        view.bringSubviewToFront(noDataLabel)
        view.layout(noDataLabel).leadingTrailing().top(200)
        noDataLabel.text = "Nothing matched with your search."
    }
}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBar(searchBar: SearchBar, didClear textField: UITextField, with text: String?) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return true
    }
    
    func searchBar(searchBar: SearchBar, didChange textField: UITextField, with text: String?) {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(searchQuery(query:)), object: typedString)
        
        if let query = text, !query.isEmpty {
            typedString = query
            perform(#selector(searchQuery(query:)), with: query, afterDelay: 1)
        } else {
            if initialData.count > 0 {
                noDataLabel.isHidden = true
                searchTableView.reloadData()
            } else {
                searchGroupCategories()
            }
            hideLoading()
        }
    }
}

extension SearchViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBar.textField.text == nil || searchBar.textField.text == "" {
            return initialData.count
        } else {
            return searchedData.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if searchBar.textField.text == nil || searchBar.textField.text == "" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GroupCategoryCell") as? GroupCategoryCell
            
            let singleCategory = initialData[indexPath.row]
            cell?.catTitleLabel.text = singleCategory.name
            if let url = singleCategory.image_url {
                cell?.catImageView.kf.setImage(with: URL(string: url))
            }
            
            
            return cell!
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableViewCell") as? SearchTableViewCell
            
            let singleItem = searchedData[indexPath.row]
            cell?.resultTitleLabel.text = singleItem.title
            cell?.resultSubtitleLabel.text = singleItem.description
            
            if let url = singleItem.source_url {
                cell?.resultImageView.kf.setImage(with: URL(string: url))
            }
            
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        searchBar.resignFirstResponder()
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchBar.textField.text == nil || searchBar.textField.text == "" {
            if let contentVC = storyboard?.instantiateViewController(withIdentifier: "ImageContentViewController") as? ImageContentViewController {
                let grp = groups[selectedButton?.tag ?? 0]
                contentVC.groupSlug = grp.slug
                
                let singleCategory = initialData[indexPath.row]
                contentVC.categorySlug = singleCategory.slug
                contentVC.categoryName = singleCategory.name
                navigationController?.pushViewController(contentVC, animated: true)
            }
        }
    }
}

class SearchTableViewCell: TableViewCell {
    
    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var resultTitleLabel: UILabel!
    @IBOutlet weak var resultSubtitleLabel: UILabel!
    
    override func prepare() {
        super.prepare()
        
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        resultImageView.cornerRadiusPreset = .cornerRadius1
        resultImageView.clipsToBounds = true
        resultImageView.backgroundColor = UIColor.hexStringToUIColorWithAlpha(hexString: ColorConstants.AppColor.BaseViewColor, alpha: 0.5)
        dividerColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.BaseViewColor)
        dividerThickness = 1
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

class GroupCategoryCell: TableViewCell {
    
    @IBOutlet weak var catImageView: UIImageView!
    @IBOutlet weak var catTitleLabel: UILabel!
    
    override func prepare() {
        super.prepare()
        
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        catImageView.cornerRadiusPreset = .cornerRadius1
        catImageView.backgroundColor = UIColor.hexStringToUIColorWithAlpha(hexString: ColorConstants.AppColor.BaseViewColor, alpha: 0.5)
        dividerColor = UIColor.hexStringToUIColor(hexString: ColorConstants.AppColor.BaseViewColor)
        dividerThickness = 1
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}



//MARK:- SearchDataModel

class SearchedData {
    
    var id: String?
    var user_id: Int?
    var group_id: Int?
    var title: String?
    var description: String?
    var tags: [JSON]?
    var status: String?
    var is_exclusive: Int?
    var source_url: String?
    
    init(jsonData: JSON) {
        
        id = jsonData["id"].stringValue
        user_id = jsonData["user_id"].intValue
        group_id = jsonData["group_id"].intValue
        title = jsonData["title"].stringValue
        description = jsonData["description"].stringValue
        tags = jsonData["tags"].arrayValue
        status = jsonData["status"].stringValue
        is_exclusive = jsonData["is_exclusive"].intValue
        source_url = jsonData["source_url"].stringValue
    }
}
