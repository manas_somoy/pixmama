//
//  SignInViewController.swift
//  Uthabo
//
//  Created by UTHABO on 22/9/18.
//  Copyright © 2018 Trinary Lab. All rights reserved.
//

import UIKit
import Material
import SwiftyJSON

class SignInViewController: BaseViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var emailTextField: CommonTextField!
    @IBOutlet weak var passwordTextfield: TextField!
    @IBOutlet weak var signInButton: FlatButton!
    @IBOutlet weak var forgotPasswordButton: FlatButton!
    @IBOutlet weak var registerLabel: UILabel!
    @IBOutlet weak var registerButton: FlatButton!
    @IBOutlet weak var skipButton: FlatButton!
    var fromInitialView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareNavigationItem()
        setContainerView()
        prepareTextViews()
        prepareButtons()
        prepareRegister()
        
        request?.delegate = self
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isMovingFromParent {
            
            if fromInitialView {
                navigationController?.setNavigationBarHidden(true, animated: false)
            } else {
                menuButton.isHidden = false
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @IBAction func signInPressed(_ sender: Button) {
        emailTextField.text = "user@demo.com"
        passwordTextfield.text = "aaaaaa"
        showHUD()
        DispatchQueue.main.async {
            self.request?.performSignInWith(Email: self.emailTextField.text ?? "", Password: self.passwordTextfield.text ?? "")
        }
    }
    @IBAction func skipButtonPressed(_ sender: Button) {
        if fromInitialView {
            delegate?.SetupTabBarController()
            delegate?.changeRootToTabVC()
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func forgotButtonPressed(_ sender: Button) {
        if let verifyVC = storyboard?.instantiateViewController(withIdentifier: "ChangePasswordViewController") as? ChangePasswordViewController {
//            navigationController?.pushViewController(verifyVC, animated: true)
        }
    }
    
    
    @IBAction func registerPressed(_ sender: Any) {
        
        let signUpVc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        signUpVc.fromInitialView = fromInitialView
        navigationController?.pushViewController(signUpVc, animated: true)
    }
    
}

extension SignInViewController {
    
    fileprivate func prepareNavigationItem() {
        if fromInitialView {
            navigationController?.setNavigationBarHidden(false, animated: true)
            navigationController?.navigationBar.barTintColor = Color.red.darken1
            navigationController?.navigationBar.tintColor = UIColor.white
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: .bold)]
            navigationItem.title = "Welcome"
        } else {
            navigationItem.titleLabel.text = "PIXMAMA"
            navigationItem.detailLabel.text = "Login"
            menuButton.isHidden = true
        }
    }
    
    fileprivate func setContainerView() {
        containerView.cornerRadiusPreset = .cornerRadius4
        containerView.depthPreset = .depth2
        containerView.depth.radius = 10
    }
    
    fileprivate func prepareTextViews() {
        emailTextField.placeholder = "Email"
        passwordTextfield.placeholder = "Password"
    }
    
    fileprivate func prepareButtons() {
        signInButton.setTitleColor(UIColor.white, for: .normal)
        signInButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        signInButton.backgroundColor = Color.red.base
        signInButton.cornerRadiusPreset = .cornerRadius2
        
        forgotPasswordButton.setTitleColor(UIColor.white, for: .normal)
        forgotPasswordButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        forgotPasswordButton.backgroundColor = Color.red.darken2
        forgotPasswordButton.cornerRadiusPreset = .cornerRadius2
        
        registerButton.setTitleColor(UIColor.white, for: .normal)
        registerButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        registerButton.backgroundColor = Color.red.base
        registerButton.cornerRadiusPreset = .cornerRadius2
        
        skipButton.setTitleColor(UIColor.white, for: .normal)
        skipButton.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        skipButton.backgroundColor = Color.deepPurple.darken1
        skipButton.cornerRadiusPreset = .cornerRadius2
    }
    
    fileprivate func prepareRegister() {
        registerLabel.textColor = Color.grey.darken2
        registerLabel.text = "Don't have an Account?"
    }
}

//MARK: APIRequestingDelegate
extension SignInViewController {
    
    func signInCompleted(DataDictionary dataDictionary: [String : Any]?) {
        if let dic = dataDictionary{
            let data = JSON(dic["data"]!)
            
            let token = data["access_token"].stringValue
            
            delegate?.saveBearerToken(Token: token)
            delegate?.saveUserData(UserData: data)
            delegate?.SetupTabBarController()
            delegate?.changeRootToTabVC()
        }
        hideHud()
    }
    
    func signInFailed(DataDictionary dataDictionary: [String : Any]?) {
        print(dataDictionary!)
        hideHud()
    }
}


















